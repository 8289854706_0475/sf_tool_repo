

function disableBack() {window.history.forward()};
function clearAll() {
    window.onload = disableBack();
    window.onpageshow = function (evt) {if (evt.persisted) disableBack()}
    window.history.pushState(null, "", window.location.href);        
    window.onpopstate = function() {
        window.history.pushState(null, "", window.location.href);
    };
    // history.replace("/")
    var cookieNames = document.cookie.split(/=[^;]*(?:;\s*|$)/);
    // Remove any that match the pattern
    for (var i = 0; i < cookieNames.length; i++) {
        if (/^prod_count/.test(cookieNames[i])) {
            document.cookie = cookieNames[i] + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/';
        }
    }
    document.cookie = 'sel_prod=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/';
    return true;
};

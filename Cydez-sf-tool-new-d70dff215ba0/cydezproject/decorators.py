from re import T
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required

def my_login_required(function):
    def wrapper(request, *args, **kw):
        user=request.user  
        if user.is_anonymous:
                return HttpResponseRedirect("/")
        decorated_view_func = login_required(request,login_url="/")
        if not user.is_authenticated:
            return decorated_view_func(request,login_url="/")  # return redirect to signin
        else:
            return function(request, *args, **kw)
    
    return wrapper
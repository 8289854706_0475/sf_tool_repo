#========================================
# Scheduler Jobs
#========================================
from apscheduler.schedulers.background import BackgroundScheduler
from pytz import utc
scheduler = BackgroundScheduler()
scheduler.configure(timezone=utc)

# jobs
import scheduler_jobs

# scheduler.add_job(scheduler_jobs.weeklymail, 'interval', seconds=10)
#scheduler.add_job(scheduler_jobs.weeklymail, trigger='cron', hour='14', minute='0')

# day_of_week 0 indicate Mon
scheduler.add_job(scheduler_jobs.weeklymail, trigger='cron', hour='0', minute='0',day_of_week='5')
# For daily job
scheduler.add_job(scheduler_jobs.timesheetRemindeSchedule, trigger='cron', hour='0', minute='0')
scheduler.start()

#========================================
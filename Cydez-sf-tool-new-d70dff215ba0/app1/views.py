from email import message
import re
from django.contrib.auth.decorators import login_required
from telnetlib import STATUS
from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse, request
from django.views.decorators.csrf import csrf_exempt
from numpy import not_equal

from cydezproject.decorators import my_login_required
from .models import Attachment, Departments, Designations, EmailConfig,Knowledge, KnowledgeLink, Logo,TaskSchedular, User,Estimate,Client,Payment,Incentives,Expenditure,Engagement,Estimation,Productdetails,My_task,Departments
from developers.models import Task
from django.contrib import messages
from urllib import response
from django.contrib.auth import login,logout,authenticate
from django.conf import settings
from django.core.mail import send_mail
from django.core.mail  import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.files.storage import FileSystemStorage

import random
import pandas as pd 
import os 
import getpass
import xlsxwriter
from datetime import datetime
from datetime import date
from datetime import timedelta
from pathlib import Path
from django.http import HttpResponseRedirect
from django.utils.dateparse import parse_date
import calendar
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.db.models import Q
from django.template.loader import render_to_string 
from django.contrib.auth.hashers import make_password
from django.template.loader import get_template
from app1.utils import getMissingUserDetails, render_to_pdf, shopUserHome,topdf,printoncertificate,date_convert, updateData
import mimetypes
from io import BytesIO
import apscheduler.schedulers.background
import xlwt
from django.core.mail import EmailMessage
import schedule
import time
from .decorator import allowed_users
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives, get_connection

logos=Logo.objects.all()

#BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = Path(__file__).resolve().parent.parent
from django.views.decorators.cache import cache_control

@cache_control(no_cache=True, must_revalidate=True)
def userlogout(request):
    
    response = HttpResponseRedirect('/')
    for cookie in request.COOKIES:
        response.delete_cookie(cookie)
    logout(request)
    request.session.flush()
    return response

## user login ##
def loginf(request):
    if request.method=='POST':       
        phone=request.POST.get('phone')
        password=request.POST.get('password')
        user = authenticate(phone=phone, password=password)
        try:
            remember = request.POST['remember_me']
            if remember:
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
        except:
            is_private = False
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
        request.session.set_expiry(1800)
        u = User.objects.filter(phone=phone,otp=password).exists()                    
        if u and user is not None:
            r=User.objects.filter(phone=phone,otp=password)
            status=r[0].status
            if status=="Active":
                role=r[0].role
                last_login=r[0].last_login
                if last_login:
                    if role == "Employee":
                        login(request,user)
                        return redirect('listtask')
                    elif role == "Consultant":
                        userid=r[0].id
                        login(request,user)
                        return redirect('consultatntbilling',userid=userid)
                    elif role == "Admin":
                        login(request,user) 
                        return redirect('dashboard')
                elif not last_login:
                    return redirect('resetpassword',userid=r[0].id)
                else:
                    messages.info(request," Invalid Phone Number or Password")
                    return render(request,'login.html',{'logos':logos})
            else:
                messages.info(request,"Account is Inactive")
                return render(request,'login.html',{'logos':logos})
        else:
            messages.info(request," Invalid Phone Number or Password")
            return render(request,'login.html',{'logos':logos})
    
    return shopUserHome(request)

def adminlogin(request):
    if request.method=='POST':
        username=request.POST.get('name')
        password=request.POST.get('psw')
        user = authenticate(phone=username, password=password)
        try:
            remember = request.POST['remember_me']
            if remember:
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
        except:
            is_private = False
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True 
        request.session.set_expiry(1800)
        u = User.objects.filter(phone=username,otp=password).exists() 
        if  u and user is not None:
            r=User.objects.filter(phone=username,otp=password) 
            status=r[0].status
            if status=="Active": 
                role=r[0].role
                if role == "Admin":
                    login(request,user) 
                    return redirect('dashboard')
                else:
                    messages.info(request,"Invalid Phone Number or Password") 
                    return render(request,'log.html')
            else:
                messages.info(request,"Account is Inactive") 
                return render(request,'log.html')
        elif  username == "9999900000" and password == "123456" : 
            # r=User.objects.filter(phone=username,otp=password)
            email="admin@gamil.com"
            user=User.objects.filter(email=email)
            return redirect('dashboard')
        else:
            messages.info(request," Invalid Phone Number or Password")
            return render(request,'log.html')
    return render(request,'log.html',{'logos':logos})


def clientlogin(request):
    if request.method=='POST':
        email=request.POST.get('name')
        password=request.POST.get('psw')
        try:
            remember = request.POST['remember_me']
            if remember:
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
        except:
            is_private = False
            settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
        request.session.set_expiry(1800)
        # u = User.objects.filter(email=email,password=password).exists()
        if email=="client@gmail.com" and password=="client":
            return render(request,'client_billing.html')
        else:
            messages.info(request," Invalid Email Address or Password")
            return render(request,'client_login.html')
    return render(request,'client_login.html')

##reset and forgot password##
def resetpassword(request,userid):
    u=User.objects.filter(id=userid).exists 
    if u:
        user=User.objects.get(id=userid)
            
        if request.method =='POST' :              
            number=request.POST.get('number') 
          
            existing_password=request.POST.get('password')
            newpass=request.POST.get('newpassword')
            confirmpassword=request.POST.get('confirmpassword') 
            u = User.objects.filter(id=userid, otp=existing_password).exists() 
            if u:
                if newpass==confirmpassword:    
                    
                    user.otp=newpass
                    user.password = make_password(newpass)
                    user.save() 
                     # Login in logout is doing to update last_login date, so on next time login rest option won't ask 
                    login(request,user) 
                    logout(request)    
                    messages.success(request,"Password reset successfully, please login again")
                    return render(request,'login.html')   
                else:
                    messages.info(request,"New and confirm password not matching")
                    
            else:
                messages.info(request,"Existing password not matching")
                
        return render(request,'setnewpassword.html',{'logos':logos,'number':user.phone,'id':user.id})
    else:
        messages.info(request,"User not exit")
        return render(request,'login.html',{'logos':logos})   

def forgotPassword(request):
    e_mail=request.POST.get('useremailId','') 
    otp=random.randint(000000,999999)
    data={'email':e_mail}

    if request.method =='POST' and 'send_otp' in request.POST:  
        u = User.objects.filter(email=e_mail).exists() 
        if u:                  
            user = User.objects.get(email=e_mail) 
            
            subject, from_email, to = 'New OTP',settings.EMAIL_HOST_USER,e_mail
            html_content = render_to_string('useremailotp.html', {'user': user,'otp':otp} )
            text_content = strip_tags(html_content)
            user.temp_otp=otp
            user.save()
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()

            messages.success(request,"OTP send successfully")
            return render(request,'resetpage.html',{'email':user.email})
        else:
            messages.error(request,"Email id not exist in our system")
 
    if request.method =='POST' and 'reset_btn' in request.POST:   
        _otp=request.POST.get('otp')
        newpass=request.POST.get('newpassword')
        confirmpassword=request.POST.get('confirmpassword')    
        data={'email':e_mail,'otp':_otp,'newpass':newpass} 
        if _otp and e_mail and newpass and confirmpassword:
                  
            u = User.objects.filter(email=e_mail, temp_otp=_otp).exists() 
            if u:
                if newpass==confirmpassword:    
                    user = User.objects.get(email=e_mail, temp_otp=_otp)
                    user.otp=newpass
                    user.password = make_password(newpass)
                    user.save() 
            
                    messages.success(request,"Password reset successfully, please login again")
                    return render(request,'login.html')   
                else:
                    messages.info(request,"New and confirm password not matching")
                    
            else:
                messages.info(request,"Incorrect email or otp")
        else:
                messages.info(request,"Please enter valid details")
           
    return render(request,'resetpage.html',{'logos':logos,'data':data})

@my_login_required

def dashboard(request):
   
    return render (request,'dashboard.html',{'logos':logos})

@my_login_required
def home(request):
   
    return shopUserHome(request)

@my_login_required
@allowed_users(allowed_roles=['Admin'])
def listUser(request):
    Dep=Departments.objects.all()
    Des=Designations.objects.all()
    activeadmin=list(User.objects.filter(role="Admin"))
    activeusers=list(User.objects.filter(role="Employee"))
    Asg = User.objects.filter(status='Active',role='Employee')
    print(Asg)
    # inactive=list(User.objects.filter(status="Inactive"))
    data=User.objects.filter(status='Active').exclude(role="Consultant")
    # datas_list=activeadmin  + activeusers 
    datas_list=User.objects.filter(status='Inactive').exclude(role="Consultant")
    query = request.GET.get('q')
    if query:
        datas_list2 = list(User.objects.filter(first_name__contains=query,role="Admin",status="Active"))
        datas_list1 = list(User.objects.filter(first_name__contains=query,role="Employee",status="Active"))
        datas_list3 = list(User.objects.filter(skill__contains=query,role="Admin",status="Active"))
        datas_list4 = list(User.objects.filter(skill__contains=query,role="Employee",status="Active"))
        data=list(set(datas_list1 + datas_list2 + datas_list3 + datas_list4))
        if not data:
            messages.info(request,"No Records Found") 
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 20)
    try:
        datas = paginator.page(page) 
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    
    paginator = Paginator(data, 20)
    try:
        data = paginator.page(page) 
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request,'user.html',{'logos':logos,'datas': datas,'data':data,'messeges':messages,'Dep':Dep,'Des':Des,'Asg':Asg})

def resend_password(request):
    if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        p=request.POST.get('otp_send')
        
        # user=User.objects.create_user(phone=phone)
        subject, from_email, to = 'Login Credentials',settings.EMAIL_HOST_USER,email
        html_content = render_to_string('resendemail.html',{'first_name':name,'phone':phone,'otp':p}) 
        text_content = strip_tags(html_content) # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        return redirect('listUser')
    
@my_login_required   

def addUser(request):
    datas=User.objects.all()
    Des=Designations.objects.all()
    Dep=Departments.objects.all()
    Asg = User.objects.filter(status='Active',role='Employee')
    
    if request.method=='POST':
        
        name=request.POST.get('name')
        phone=request.POST.get('Phone')
        email=request.POST.get('email')
        email1=request.POST.get('email1')
        salary=request.POST.get('salary')
        billing=request.POST.get('billing')
        designation=request.POST.get('designation')
        department=request.POST.get('department')
        status='Active'
        role=request.POST.get('role')
        
        date=request.POST.get('date')
        rank=request.POST.get('rank')
        skill=request.POST.get('skill') 
        asignee = request.POST.get('asignee') 
        a = User.objects.get(id= asignee)
       
        try:
            resume=request.FILES['st_doc']
            if not resume:
                resume=None
        except:
            resume=None
        f1=FileSystemStorage()
        if resume:
           f2=f1.save(resume.name,resume)
        
        if User.objects.filter(phone=phone).exists():
            messages.error(request,'Phone Number already exists!')
            return redirect('listUser')
        if User.objects.filter(email=email).exists():
            messages.error(request,' Email Id already exists!')
            return redirect('listUser')
        # if User.objects.filter(phone=phone).exists():
        #     messege="Phone Number already exists"
        #     k={'status':status,"role":role,"name":name,'email':email,'salary':salary, 'billing':billing,'designation':designation,'department':department,'email1':email1,'rank':rank,'skill':skill,'resume':resume}
        #     return render(request,'user.html',{'messege':messege,'userdetails':k,'datas':datas})
        otp=random.randint(000000,999999)
        sb=int(salary)*int(billing)
        z=sb/25
        x=z/8
        department =request.POST.get('department')
        p=Departments.objects.get(id=department)
        designation =request.POST.get('designation')
        q=Designations.objects.get(id=designation)
        user=User.objects.create_user(status=status,role=role,first_name=name,phone=phone,email=email,salary=salary,billing=billing,designation=q,department=p,amount=x,otp=otp,password=str(otp),officemail=email1,date=date,rank=rank,skill=skill,resume=resume,asignee=a)
       
        subject, from_email, to = 'Created Successfully',settings.EMAIL_HOST_USER,user.email
        html_content = render_to_string('useremail.html', {'user': user,'otp':otp}) 
        #) # render with dynamic value
        text_content = strip_tags(html_content) # Strip the html tag. So people can see the pure text at least.

        # create the email, and attach the HTML version as well.
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        #subject='OTP Generated'
        message='Hi your username is {0}  and password is {1} . please login "http://sf.cydeztechnologies.com/" .Thank you'.format(user.phone,user.otp)
        #username=user.phone
        #password=user.otp
        #email_from=settings.EMAIL_HOST_USER
        #recipient_list=[user.email,user.officemail]
        #send_mail(subject,message,email_from,recipient_list)
        messages.success(request,'Created Successfully!')
        return redirect('listUser')
    return render(request,'user.html',{'logos':logos,'datas': datas,'Des':Des,'Dep':Dep,'Asg':Asg})

@my_login_required

def deleteUser(request,userid):
    k=User.objects.get(id=userid)
    k.delete()
    return redirect('listUser')

@my_login_required

def editUser(request): 
    Des=Designations.objects.all()
    Asg = User.objects.filter(status='Active',role='Employee')
    if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        salary=request.POST.get('salary')
        user_id=request.POST.get('user_id')
        billing=request.POST.get('billing')
        email1=request.POST.get('email1')
        
        rank=request.POST.get('rank')
        status=request.POST.get('status')
        role=request.POST.get('role')
        skill=request.POST.get('skill')
       
        # resume=request.FILES['st_doc']   
        # f1=FileSystemStorage()  
        # f2=f1.save(resume.name,resume)
        try:
            resume=request.FILES['st_doc']
            if not resume:
                resume=None
        except:
            resume=None
        f1=FileSystemStorage()
        if resume:
           f2=f1.save(resume.name,resume)
        else:
            resume=None
        sb=int(salary)*int(billing)
        z=sb/25 
        x=z/8  
                         
        user_id=request.POST.get("user_id")
        k=User.objects.filter(id=user_id)  
        department =request.POST.get('department')
        p=Departments.objects.get(Dname=department)
        designation =request.POST.get('designation')
        q=Designations.objects.get(Designation=designation)
        asignee = request.POST.get('asignee') 
        # a = User.objects.get(id = asignee)
        uv=User.objects.filter(email=email)
        u=User.objects.filter(phone=phone)
        if uv and uv.exists() and uv.first().id!=k.first().id:
            messages.error(request,'Email Id already exists')
        elif u and u.exists() and u.first().id!=k.first().id:
            messages.error(request,'Phone number already exists')
        else:
            k.update(status=status,role=role,first_name=name,phone=phone,department=p,email=email,salary=salary,amount=x,billing=billing,officemail=email1,designation=q,rank=rank,skill=skill,asignee=asignee)
            if resume:
                k.update(resume=resume)
            messages.success(request,'Updated Successfully!')
        return redirect('listUser')
    return render(request,'user.html',{'Asg':Asg})
## estimation ##
@my_login_required

def estimate(request):
    l= Client.objects.filter(status="Active").order_by('clientNameid')
    if request.method=='POST':
        projectname=request.POST.get('projectname')
        p = Client.objects.get(id=projectname)
        estimatedamount=request.POST.get('estimatedamount')
        paidamount=request.POST.get('paidamount')
        dueamount= int(estimatedamount)-int(paidamount)
        # dueamount=request.POST.get('dueamount')
        Estimate.objects.create(projectname=p,estimatedamount=estimatedamount,paidamount=paidamount,dueamount=dueamount)
        return redirect('listProject')
    return render(request,'estimate.html',{'logos':logos,'l':l})

@my_login_required

def editestimate(request):
    l= Client.objects.all()
    if request.method=='POST':
        estimatedamount=request.POST.get('estimatedamount')
        #paidamount=request.POST.get('paidamount')
        #dueamount= int(estimatedamount)
        user_id=request.POST.get('user_id')
        k=Estimate.objects.filter(id=user_id)
        dueamount= int(estimatedamount)-int(k.first().paidamount)
        k.update(estimatedamount=estimatedamount,dueamount=dueamount)
       
        return redirect('listProject')

#email clients about the approved amount and billed amount with worker details
def auto_email_friday():
    dayf=date.today() - timedelta(days=5)
    dayt=dayf-timedelta(days=7)
    task_filter_bydate=Task.objects.filter(date__gte=dayt,date__lte=dayf,status=True)
    pro_list=[]
    for i in task_filter_bydate:
        if i.project_id not in pro_list:
            pro_list.append(i.project_id)
    for i in pro_list:
        emp_list=[]
        emp_details=[]
        task_filter_byproid=task_filter_bydate.filter(project_id=i)
        fgh=Estimate.objects.get(projectname_id=i)
        billd_amount=fgh.billed_amount
        appr_amt=0
        for j in task_filter_byproid:
            appr_amt=appr_amt+j.amount
        client_var=Client.objects.get(id=i)
        for e in task_filter_byproid:
            if e.employee not in emp_list:
                emp_list.append(e.employee)
        for m in emp_list:
            task_filter_byemp_list=task_filter_byproid.filter(employee=m)
            work_hours=[]
            for i in task_filter_byemp_list:
                work_hours=work_hours=i.hours
            emp_details.append({"Employee_Name":m,"Hours_Spend":work_hours})
        emp_table=pd.DataFrame(emp_details)
        date_tod=date.today()
        html_content = render_to_string("client_automail.html",{'client_name':client_var.clientName,"emp_details":emp_details,"dayf":dayf,"dayt":dayt,"appr_amt":appr_amt,"date_tod":date_tod,"billd_amount":billd_amount})
        text_content=strip_tags(html_content)
        subject='Cydez Technologies'
        message='Hi  {0},your approved amount from {1} to {2} is Rs.{3} and billed amount as on {4} is Rs.{5}. \n '.format(client_var.clientName,dayf,dayt,appr_amt,date_tod,billd_amount)
        email_from=settings.EMAIL_HOST_USER
        recipient_list=["christojohnson96@gmail.com"]
        email = EmailMultiAlternatives(
            subject,
            text_content,
            email_from,
            recipient_list,
        )
        email.attach_alternative(html_content,"text/html")
        email.send()

#automatic task scheduled on friday 10am
def start():
    scheduler=apscheduler.schedulers.background.BackgroundScheduler()
    scheduler.add_job(auto_email_friday,'cron',day_of_week='fri', hour='10',minute='00')
 
    scheduler.add_job(auto_taskschedular, 'cron',hour='11', minute='25')
    scheduler.start()


@my_login_required
@allowed_users(allowed_roles=['Admin'])
def listProject(request):
    l= Client.objects.all()
    data=Estimate.objects.filter(projectname__status='Active')
    if not data:
        messages.info(request,'No Records Found')
    datas_list=list(Estimate.objects.filter(projectname__status='Completed'))
    # list1 = activeprojects
    # datas_list = list1
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)

    paginator = Paginator(data, 20)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

       
### for search project ###
    s=request.POST.get('search')
    if s:        
        data=Estimate.objects.filter(projectname__clientName__contains=s,projectname__status='Active')
        if not data:
            messages.info(request,"No Records Found")
        # page = request.GET.get('page', 1)
        # paginator = Paginator(datas_list, 20)
        # try:
        #     datas = paginator.page(page)
        # except PageNotAnInteger:
        #     datas = paginator.page(1)
        # except EmptyPage:
        #     datas = paginator.page(paginator.num_pages)
        return render(request,'estimate.html',{'messeges':messages,'datas':datas,'data':data,'s':s})
    return render(request,'estimate.html',{'logos':logos,'datas':datas,'data':data,'l':l})

@my_login_required

def deleteProject(request,projectid): 
    k=Estimate.objects.get(id=projectid)
    k.delete()
    return redirect('listProject')

@my_login_required
@allowed_users(allowed_roles=['Admin'])
def task(request):
    k1=list(User.objects.filter(status="Active" , role="Employee").exclude(role="Consultant"))
    k2=list(User.objects.filter(status="Active" , role="Admin"))
    k= k1 + k2
    datas_list=My_task.objects.all().order_by('-end_date').exclude(assignee=request.user) 
    # datas_list=My_task.objects.filter(created_by=request.user).order_by("-end_date")
    e=request.GET.get('users')  
    # c=request.GET.get('usr')   
    a=request.GET.get('start_date') 
    b=request.GET.get('end_date') 
    s=request.GET.get('status')

    path=os.path.join(BASE_DIR,'media/csv/taskdetails.xlsx')
    loop_count = 1
    writer = pd.ExcelWriter(path, engine='xlsxwriter')
    search_variables = {}
   
    if e : 
        search_variables['assignee__contains']=e  
    # if c : 
    #     search_variables['created_by__contains']=c   
    if s:
        search_variables['status__contains']=s   
    if a: 
        search_variables['end_date__gte']=a
    if b: 
        search_variables['end_date__lte']=b

    datas_list=My_task.objects.filter(**search_variables).order_by('-end_date').exclude(assignee=request.user) 

    datas=[]
    data = My_task.objects.filter(**search_variables)
    for i in data:
        task = [i.title, i.description , i.end_date , i.assignee , i.status] 
        datas.append(task)
        pass

    df = pd.DataFrame(item for item in datas)

    df.rename(columns={0: 'Title', 1: 'Description', 2: 'Final Date', 3: 'assignee',
              4: 'status'}, inplace=True)

    loop_count += 1
    df.to_excel(writer, sheet_name='Sheet' +
                str(loop_count), index=False)

    ## for adjust width ###
    worksheet = writer.sheets['Sheet' + str(loop_count)]
    for idx, col in enumerate(df):
        series = df[col]
        max_len = max((series.astype(str).map(
            len).max(), len(str(series.name))))+1
        worksheet.set_column(idx, idx, max_len)

    writer.save()

    if not datas_list:
        messages.info(request,"No Records Found")    
   
    paginator = Paginator(datas_list, 20) # 6 datas per page
    page = request.GET.get('page',1) 
    try:
        datas = paginator.page(page)
    except PageNotAnInteger: 
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'task.html',{'logos':logos,'k':k,'datas':datas}) 

@my_login_required
@allowed_users(allowed_roles=['Admin'])
def mytask(request):
    k1=list(User.objects.filter(status="Active" , role="Employee").exclude(role="Consultant"))
    k2=list(User.objects.filter(status="Active" , role="Admin"))
    k= k1 + k2
      
    # datas_list=My_task.objects.all().order_by('-end_date') 
    datas_list=My_task.objects.filter(asignee=request.user).order_by("-end_date")
    e=request.GET.get('users')     
    a=request.GET.get('start_date')
    b=request.GET.get('end_date') 
    s=request.GET.get('status')

    search_variables = {}
   
    if e : 
        search_variables['created_by__contains']=e    
    if s:
        search_variables['status__contains']=s   
    if a: 
        search_variables['end_date__gte']=a
    if b: 
        search_variables['end_date__lte']=b
   
    datas_list=My_task.objects.filter(**search_variables,asignee=request.user).order_by('-end_date') 
    if not datas_list:
        messages.info(request,"No Records Found")    
   
    paginator = Paginator(datas_list, 20) # 6 datas per page
    page = request.GET.get('page',1) 
    try:
        datas = paginator.page(page)
    except PageNotAnInteger: 
        datas = paginator.page(1)
    except EmptyPage: 
        datas = paginator.page(paginator.num_pages)
    return render(request,'mytask.html',{'logos':logos,'k':k,'datas':datas}) 
 
@my_login_required

def add_task(request):
    # user=request.user
    if request.method=='POST':
        title=request.POST.get('title')
        description=request.POST.get('description')
        end_date=request.POST.get('date')
        start_date=date.today()
        asignee=request.POST.get('asignee') 
        created_by=request.POST.get('created')         
        try:
            attachment=request.FILES['attachment']
            if not attachment:
                attachment=None
        except:
            attachment=None
        f1=FileSystemStorage()
        if attachment:
         f2=f1.save(attachment.name,attachment)    
        My_task.objects.create(title=title,description=description,end_date=end_date,start_date=start_date,asignee=asignee,status="New", created_by=created_by ,attachment=attachment)
        return redirect('task') 
    return render(request,'task.html')

@my_login_required
def edit_task(request):
    if request.method=='POST':
        title=request.POST.get('title')
        description=request.POST.get('description')
        end_date=request.POST.get('date')
        asignee=request.POST.get('asignee') 
        status=request.POST.get('status')
        try:
            attachment=request.FILES['attachment']
            if not attachment:
                attachment=None
        except:
            attachment=None
        f1=FileSystemStorage()
        if attachment:
         f2=f1.save(attachment.name,attachment)   
        mytaskid=request.POST.get('mytaskid') 
        k=My_task.objects.filter(id=mytaskid)  
        k.update(title=title,description=description,end_date=end_date,asignee=asignee,status=status)
        if attachment:
            k.update(attachment=attachment)
        return redirect('task') 
    return render(request,'task.html')

@my_login_required

def delete_Task(request,my_taskid): 
    k=My_task.objects.get(id=my_taskid)
    k.delete() 
    return redirect('task')

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def task_employee(request):
    # user= request.user
    # if user.is_authenticated :
    k1=list(User.objects.filter(status="Active" , role="Employee").exclude(role="Consultant"))
    k2=list(User.objects.filter(status="Active" , role="Admin"))
    k= k1 + k2
    e=request.GET.get('user')
    datas_list=My_task.objects.filter(created_by=request.user).order_by("-end_date").exclude(asignee=request.user)
    data=My_task.objects.filter(asignee=request.user).order_by("-end_date")
    paginator = Paginator(datas_list, 20) # 6 datas per page
    page = request.GET.get('page',1) 
    try:
        datas = paginator.page(page)
    except PageNotAnInteger: 
        datas = paginator.page(1)
    except EmptyPage:   
        datas = paginator.page(paginator.num_pages)  

    paginator = Paginator(data, 20)
    try:
        data = paginator.page(page)
    except PageNotAnInteger: 
        data = paginator.page(1)
    except EmptyPage:   
        data = paginator.page(paginator.num_pages ) 
    return render(request,'taskemployee.html',{'logos':logos,'k':k,'data':data,'e':e,'datas':datas})

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def employeetask_add(request):
    if request.method=='POST':
        title=request.POST.get('title')
        description=request.POST.get('description')
        end_date=request.POST.get('date')
        start_date=date.today()
        asignee=request.POST.get('asignee') 
        created_by = request.POST.get('created') 
        try:
            attachment=request.FILES['attachment']
            if not attachment:
                attachment=None
        except:
            attachment=None
        f1=FileSystemStorage()
        if attachment:
           f2=f1.save(attachment.name,attachment)   
        My_task.objects.create(title=title,description=description,end_date=end_date,start_date=start_date,asignee=asignee,status="New", created_by=created_by ,attachment=attachment)
        return redirect('task_employee') 
    # return render(request,'taskemployee.html')

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def edit_employeetask(request):
    if request.method=='POST':
        title=request.POST.get('title')
        description=request.POST.get('description')
        end_date=request.POST.get('date')
        asignee=request.POST.get('asignee') 
        status=request.POST.get('status')
        try:
            attachment=request.FILES['attachment']
            if not attachment:
                attachment=None
        except:
            attachment=None
        f1=FileSystemStorage()
        if attachment:
           f2=f1.save(attachment.name,attachment)
        mytaskid=request.POST.get('mytaskid') 
        k=My_task.objects.filter(id=mytaskid)  
        k.update(title=title,description=description,end_date=end_date,asignee=asignee,status=status)
        if attachment:
            k.update(attachment=attachment)
        return redirect('task_employee') 
    # return render(request,'taskemployee.html')

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def edit_employeemytask(request):
    if request.method=='POST':
        title=request.POST.get('title')
        status=request.POST.get('status')
        try:
            attachment=request.FILES['attachment']
            if not attachment:
                attachment=None
        except:
            attachment=None
        f1=FileSystemStorage()
        if attachment:
         f2=f1.save(attachment.name,attachment)
        mytaskid=request.POST.get('mytaskid') 
        k=My_task.objects.filter(id=mytaskid)  
        k.update(title=title,status=status)
        return redirect('task_employee') 

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def delete_empTask(request,my_taskid): 
    k=My_task.objects.get(id=my_taskid)
    k.delete() 
    return redirect('task_employee')

####client ###########
@my_login_required
def create_client(request):
    if request.method == 'POST':
        clientName = request.POST.get('clientName')
        email = request.POST.get('email')
        optionalemail = request.POST.get('email1')
        status = 'Active'
        weeklyMail = request.POST.get('weeklymail')
        if weeklyMail == "on":
            weeklyMail = True
        else:
            weeklyMail=False
        parent =request.POST.get('parent')
        if parent!= "None":
            p=Engagement.objects.get(id=parent)
            c = Client.objects.create(weeklyMail=weeklyMail,clientName=clientName, email=email,optionalemail=optionalemail,status=status,Parent=p)
            Estimate.objects.create(projectname= c)
        else:
            c = Client.objects.create(weeklyMail=weeklyMail,clientName=clientName, email=email,optionalemail=optionalemail,status=status)
            Estimate.objects.create(projectname= c)
    return redirect('listclient')

@my_login_required
def searchclient(request):
    datas=Client.objects.all()
    if request.method=='POST':
        s=request.POST.get('search')
        datas=datas.filter(clientName__contains=s)
        return render(request, 'client.html',{'logos':logos,'datas':datas})

@my_login_required    
@allowed_users(allowed_roles=['Admin'])
def listclient(request):
    parentproject=Engagement.objects.filter(status="Active")
    datas_list = Client.objects.filter(status='Completed')
    data=Client.objects.filter(status="Active")
    if not data:
        messages.info(request,'No Records Found')
    query = request.GET.get('q')
    if query:
        data = Client.objects.filter(clientName__contains=query,status="Active")
        if not data:
            messages.info(request,"No Records Found")
    page = request.GET.get('page',1)
    paginator = Paginator(datas_list, 20) # 6 datas per page
    
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    paginator = Paginator(data, 20)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request, "client.html",{'logos':logos,'datas':datas,'data':data,'parentproject':parentproject,'messeges':messages})
    
@my_login_required
def editClient(request):
    if request.method=='POST':
        clientName=request.POST.get('clientName')
        client_id=request.POST.get('client_id')
        k=Client.objects.filter(id=client_id)
        email=request.POST.get('email')
        optionalemail = request.POST.get('email1')
        status=request.POST.get('status')
        weeklyMail = request.POST.get('weeklymail')
        if weeklyMail == "False":
            weeklyMail = True
        else:
            weeklyMail=False
        parent =request.POST.get('parent')
        if parent!= "None":
            p=Engagement.objects.get(engagementName=parent)
            k.update(weeklyMail=weeklyMail,clientName=clientName,email=email,optionalemail=optionalemail,status=status,Parent=p)
            return redirect('listclient')
        else:
            k.update(weeklyMail=weeklyMail,clientName=clientName,email=email,optionalemail=optionalemail,status=status)
            return redirect('listclient')
@my_login_required
def client_billing(request):
    return render(request,"client_billing.html")

@my_login_required
def client_estimation(request):  
    return render(request,"client_estimation.html")

@my_login_required
def client_accountmanager(request):  
    return render(request,"client_accountmanager.html")

@my_login_required
def client_generatestimate(request):  
    return render(request,"client_generatestimate.html")

@my_login_required
def deleteclient(request,id):
    u=Client.objects.get(id=id)
    u.delete()
    return redirect('listclient')

@my_login_required
def accept(request,task_id):
    if task_id:
        bs = Task.objects.get(id=task_id) 
        print(bs,"............bsbsbs")
        if request.GET.get('approve')=='true':            
            bs.status = True
            print(bs.status,"status")
            bs.save()
        elif request.GET.get('approve')=='false':
            bs.status = False
            print(bs.status,"status")
            bs.save()
    l=Client.objects.all()
    k=User.objects.all()
    datas_list=Task.objects.all().order_by('-date')
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 50)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,"totalbillingsearch.html",{'logos':logos,'datas':datas,'l':l,'k':k,'task_id':task_id})
@csrf_exempt
@my_login_required
def approve_all(request):
    today = date.today()
    d=today.day
    dd = today - timedelta(days = 2) 
    tk = Task.objects.filter(status=None).filter(date__gte= dd)
    if tk:
        for i in tk:
            i.status = True
            project = i.project
            if hasattr(project,'estimate'):
                estimate = i.project.estimate
                try:
                    estimate.billed_amount += i.amount
                except:
                    estimate.billed_amount=0
                    estimate.billed_amount += i.amount
                    pass
                estimate.save()
                engagement=i.project.Parent
                if engagement!=None:
                    engagements=Engagement.objects.get(engagementName=engagement)
                    engagements.engagementValue += i.amount
                    engagements.save()
                    user=User.objects.filter(engagement=engagement,status="Active").values()
                    for u in user:
                        k=u['id']
                        user=User.objects.get(id=k)
                        user.value += i.amount
                        user.save()
                estimate.save()
            i.save()
            print(estimate.billed_amount)
    return redirect(Totalbilling)

   
@my_login_required   
@allowed_users(allowed_roles=['Admin'])
def pending(request):
    
    query=request.POST.get('search')
    users_data=getMissingUserDetails(query)
          
    if query:
        s=query
    else:
        s=''
    from collections import OrderedDict
    users_data = OrderedDict(sorted(users_data.items()))
    if not users_data:
        messages.info(request,'No Records Found')
    page = request.GET.get('page', 1)
    items = list(users_data.items())
    paginator = Paginator(items, 10)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    print(datas)
    return render(request,'pending_task.html',{'logos':logos,'datas':datas,'messeges':messages,'s':s})



@csrf_exempt
@my_login_required
@allowed_users(allowed_roles=['Admin'])
def Totalbilling(request): 
    df=pd.DataFrame(Task.objects.all().values())
    l=Client.objects.filter(status="Active")
    k=User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    datas_list=Task.objects.all()
    if request.method == 'POST':
        task_id = request.POST.get('taskId')
        status = int(request.POST.get('status'))
        bs = Task.objects.get(id=task_id)
        if status:
            if not bs.status:
                bs.status = True
                if bs.amount:
                    project = bs.project
                    if hasattr(project,'estimate'):
                        estimate = bs.project.estimate
                        try:
                            estimate.billed_amount += bs.amount
                        except:
                            estimate.billed_amount=0
                            estimate.billed_amount += bs.amount
                            pass
                        engagement=bs.project.Parent
                        if engagement!=None:
                            engagements=Engagement.objects.get(engagementName=engagement)
                            engagements.engagementValue += bs.amount
                            engagements.save()
                            user=User.objects.filter(engagement=engagement,status="Active").values()
                            for i in user:
                                k=i['id']
                                user=User.objects.get(id=k)
                                user.value += bs.amount
                                user.save()
                        estimate.save()
        else:
            if bs.status == True:
                bs.status = False
                if bs.amount:
                    project = bs.project
                    if hasattr(project,'estimate'):
                        estimate = bs.project.estimate
                        estimate.billed_amount -= bs.amount 
                        engagement=bs.project.Parent
                        if engagement!=None:
                            engagements=Engagement.objects.get(engagementName=engagement)
                            engagements.engagementValue -= bs.amount
                            engagements.save()
                            user=User.objects.filter(engagement=engagement,status="Active").values()
                            for i in user:
                                k=i['id']
                                user=User.objects.get(id=k)
                                user.value -= bs.amount
                                user.save()
                        estimate.save()
            else:
                bs.status = False
        bs.save()
        return JsonResponse({"status":True,"message":"success","button":status})
    l=Client.objects.filter(status="Active")
    k=User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    datas_list=Task.objects.all().order_by('-id')
    s = request.GET.get('status')
    p=request.GET.get('name')
    e=request.GET.get('employee')
    a=request.GET.get('start_date')
    b=request.GET.get('end_date')
    path=os.path.join(BASE_DIR,'media/csv/billing.xlsx')
    loop_count = 1
    writer = pd.ExcelWriter(path, engine='xlsxwriter')
    per_total = 0
    total_amount = 0
    employees={}
    search_variables = {}
    if s:
   
        search_variables['status__contains'] = s
    if p:
    
        search_variables['project__id'] = p
    if e: 

        search_variables['employee__id'] = e
    if a:
        
        search_variables['date__lte'] = a
    if b:
       
        search_variables['date__gte'] = b
    for i in User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True):
        task = i.task_set.filter(**search_variables)
        task1 =task.values('status','amount')
        task = task.values('employee__first_name','date','day','project__clientName','taskdetails','hours','amount')

        if task:
            index=0
            for j in task1:
                if j['status'] == True:
                    per_total += j['amount']
                else:
                    task[index]['amount']=0
                o=per_total
                index+=1
            per_total=0
            
            df=pd.DataFrame(task)
            df2 = {'employee__first_name':'','date':'','day':'','project__clientName':'','taskdetails':'','hours':'Total','amount':o}
            df = df.append(df2, ignore_index = True)
            df.columns=['Employee','Date','Day','Project','Taskdetails','Hours','Amount']
            df['Date'] = pd.to_datetime(df.Date)
            df['Date'] = df['Date'].dt.strftime('%d-%m-%Y')
            
            loop_count += 1
            df.to_excel(writer, sheet_name='Sheet' + str(loop_count),index=False)
            ### for adjust width ###
            worksheet=writer.sheets['Sheet' + str(loop_count)]
            for idx,col in enumerate(df):
                series=df[col]
                max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
                worksheet.set_column(idx,idx,max_len)
            employees[i.get_full_name()]=[o]
            total_amount += o
    employees['Total']=[total_amount] 
    new_df = pd.DataFrame(employees)
    new_df.to_excel(writer, sheet_name='Sheet1',index=False)
    writer.save()
    datas_list=Task.objects.filter(**search_variables).order_by('-date')
    if not datas_list:
        messages.info(request,"No Records Found")
    page = request.GET.get('page', 1)
    
    paginator = Paginator(datas_list, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    if p:
        p=int(p)
    if e:
        e=int(e)
    return render(request,"totalbillingsearch.html",{'logos':logos,'datas':datas,'l':l,'k':k,'s':s,'p':p,'e':e,'startdate':a,'enddate':b,'search_variables':search_variables,'messeges':messages})
## payment ##
@my_login_required
@allowed_users(allowed_roles=['Admin'])
def payment(request,clientid):
    # z=Payment.objects.all()
    client=Client.objects.filter(id=clientid)
    if client:
        client=client.first()
        print(client,client.id,"client")
        datas_list=Payment.objects.filter(name=clientid)
        if not datas_list:
                messages.info(request,"No Records Found")
        page = request.GET.get('page', 1)
        paginator = Paginator(datas_list, 20)
        try:
            datas = paginator.page(page)
        except PageNotAnInteger:
            datas = paginator.page(1)
        except EmptyPage:
            datas = paginator.page(paginator.num_pages)
        return render(request,'payment.html',{'logos':logos,'datas':datas,'id':clientid,'client':client,'messeges':messages})
    else:
        return render(request, '404.html')

@my_login_required
def addPayment(request):
    if request.method=='POST':
        clientName=request.POST.get('clientid')
        name=Client.objects.get(id=clientName)
        date=request.POST.get('date')
        details=request.POST.get('details')
        amountrecieved=request.POST.get('amountrecieved')
        #total = Payment.amountreceived.aggregate(total=Sum('amountreceived'))['total'] or 0
        Payment.objects.create(name=name,date=date,details=details,amountrecieved=amountrecieved)
        estimate_obj=Estimate.objects.get(projectname_id=clientName)
     
        try:
            estimate_obj.paidamount += int(amountrecieved)
        except:
            estimate_obj.paidamount=0
            estimate_obj.paidamount += int(amountrecieved)
            pass
        dueamount= int(estimate_obj.estimatedamount)-int(estimate_obj.paidamount)
        estimate_obj.dueamount=dueamount
        estimate_obj.save()
        return redirect('payment',clientid=name.id)
    return render(request,'payment.html',{'logos':logos})

@my_login_required
def deletePayment(request,paymentid):
    p=Payment.objects.all()
    k=Payment.objects.get(id=paymentid)
    clientid=k.name.id
    amountrecieved=k.amountrecieved
    k.delete()
    estimate_obj=Estimate.objects.get(projectname_id=clientid)
     
    try:
        estimate_obj.paidamount -= int(amountrecieved)
    except:
        estimate_obj.paidamount=0
        estimate_obj.paidamount -= int(amountrecieved)
        pass
    dueamount= int(estimate_obj.estimatedamount)-int(estimate_obj.paidamount)
    estimate_obj.dueamount=dueamount
    estimate_obj.save()
    return redirect('payment',clientid=clientid)
    #return render(request,'payment.html')
    
@my_login_required
def editPayment(request):
    if request.method=='POST':
        clientName=request.POST.get('clientid')
        name=Client.objects.get(id=clientName)
        date=request.POST.get('date')
        details=request.POST.get('details')
        amountrecieved=request.POST.get('amountrecieved')
        user_id=request.POST.get('user_id')
        k=Payment.objects.filter(id=user_id)
        estimate_obj=Estimate.objects.get(projectname_id=clientName)
     
        try:
            estimate_obj.paidamount -= k.first().amountrecieved
            estimate_obj.paidamount += int(amountrecieved)
        except Exception as e:
            print(e)
            estimate_obj.paidamount=0
            estimate_obj.paidamount += int(amountrecieved)
            pass
        dueamount= int(estimate_obj.estimatedamount)-int(estimate_obj.paidamount)
        estimate_obj.dueamount=dueamount
        k.update(name=name,date=date,details=details,amountrecieved=amountrecieved)
        estimate_obj.save()
        return redirect('payment',clientid=name.id)
    return render(request,'payment.html')
### user billing ###
@my_login_required
def BillingPerfom(request,userid):
    h=0
    today = date.today().strftime("%Y-%m")
    r = date.today()
    start_date=date(int(r.year),int(r.month),1)
    k=User.objects.get(id=userid)
    print("NAME",k)
    j=User.objects.filter(id=userid)
    for i in j:
        d=i.date_joined
        s=i.salary
        b=i.billing
        total=int(s)*int(b)
        per_day=total/25
        per_hour=per_day/8
        task=i.task_set.filter(status=True,date__gte=start_date,date__lte=r)
        if task:
            for x in task:
                h +=x.hours
                total_hour=h
            h=0
        else:
            value=0
            Totalvalue=0
            messege="sorry you have no approved task in this month"
            return render(request,'justgage.html',{'messege':messege,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
    total_days_in_a_month=25
    total_working_hour_in_a_month=25*8
    numerator=per_hour*total_hour*100
    denominatior=per_hour* total_working_hour_in_a_month          
    value=numerator/denominatior
    print("VALUE",value)
### for filter by date ###
    if request.method=='POST':
        a=request.POST.get('date')
        print('date',a)
        # r=parse_date(a)
        # start_date=date(int(r.year),int(r.month),1)
        y=a.split('-')[0]
        m=a.split('-')[1]
        year=int(y)
        month=int(m)
        num_days=(calendar.monthrange(year,month)[1]);
        end_date=date(int(y),int(m),num_days)
        start_date=date(int(y),int(m),1)
        for i in j:
            d=i.date_joined
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=start_date,date__lte=end_date)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                value=0
                Totalvalue=0
                messege="sorry you have no approved task in this month"
                return render(request,'justgage.html',{'messege':messege,'k':k,'value':value,'id':userid,'Totalvalue':Totalvalue,'searchDate':a})
        total_days_in_a_month=25
        total_working_hour_in_a_month=25*8
        numerator=per_hour*total_hour*100
        denominatior=per_hour* total_working_hour_in_a_month          
        value=numerator/denominatior
        print(" value",value)
        current_date=date.today()
        end_date=date(int(current_date.year),int(current_date.month),1)
        days_before=(end_date-timedelta(days=1))
        num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
        for i in j:
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                Totalvalue=0
                return render(request,'justgage.html',{'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':a})
        numerator=per_hour*total_hour*100
        denominatior=per_hour*num_months*200
        Totalvalue=numerator/denominatior
        print("Totalvalue",Totalvalue)
        return render(request,'justgage.html',{'k':k,'value':value,'id':userid,'searchDate':a,'Totalvalue':Totalvalue})
### for total performance ###
    current_date=date.today()
    end_date=date(int(current_date.year),int(current_date.month),1)
    days_before=(end_date-timedelta(days=1))
    num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
    for i in j:
        s=i.salary
        b=i.billing
        total=int(s)*int(b)
        per_day=total/25
        per_hour=per_day/8
        task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
        if task:
            for x in task:
                h +=x.hours
                total_hour=h
            h=0
        else:
            Totalvalue=0
            return render(request,'justgage.html',{'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
    numerator=per_hour*total_hour*100
    denominatior=per_hour*num_months*200
    Totalvalue=numerator/denominatior
    print("Totalvalue",Totalvalue)
    return render(request,'justgage.html',{'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})

@my_login_required
def Incentive(request):
    first_day_of_current_month = date.today().replace(day=1)
    last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
    b=last_day_of_previous_month.strftime('%Y-%m')
    mon=last_day_of_previous_month.strftime('%B')
    year=last_day_of_previous_month.year
    k= Incentives.objects.filter(month=mon,year=year)
    if request.method =='POST':
        da = request.POST.get('searchdate')
        y = da.split('-')[0]
        m = da.split('-')[1]
        mo=date(int(y),int(m),1).strftime('%B')
        k=Incentives.objects.filter(month=mo,year=y)
        return render(request,'incentive.html',{'k':k,'selectedDate':da})
    return render(request,'incentive.html',{'k':k,'selectedDate':b})

@my_login_required
def monthly(request):
    salary=0
    amounts=0
    first_day_of_current_month = date.today().replace(day=1)
    b=first_day_of_current_month.strftime('%Y-%m')
    y=b.split('-')[0]
    m=b.split('-')[1]
    year=int(y)
    month=int(m)
    num_days=(calendar.monthrange(year,month)[1])
    end_date=date(int(y),int(m),num_days)
    e = Expenditure.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    k = Payment.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    u = User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    for i in u:
        salary += i.salary
    expense = salary
    for o in e:
        expense += o.amount
    for j in k:
        amounts += j.amountrecieved
    if request.method =='POST':
        amounts=0
        a=request.POST.get('date')
        y=a.split('-')[0]
        m=a.split('-')[1]
        year=int(y)
        month=int(m)
        num_days=(calendar.monthrange(year,month)[1])
        end_date=date(int(y),int(m),num_days)
        start_date=date(int(y),int(m),1)
        e = Expenditure.objects.filter(date__gte=start_date,date__lte=end_date)
        k = Payment.objects.filter(date__gte=start_date,date__lte=end_date)
        expense = salary
        for p in e:
            expense += p.amount
        for i in k:
            amounts += i.amountrecieved
        return render(request,'monthly.html',{'k':k,'searchDate':a,'e':e,'amounts':amounts,'salary':salary,'expense':expense,'b':b})
    return render(request,'monthly.html',{'logos':logos,'k':k,'searchDate':b,'amounts':amounts,'e':e,'salary':salary,'expense':expense,'b':b})

@my_login_required
def expenses(request):
    salary=0
    amounts=0
    first_day_of_current_month = date.today().replace(day=1)
    b=first_day_of_current_month.strftime('%Y-%m')
    y=b.split('-')[0]
    m=b.split('-')[1]
    year=int(y)
    month=int(m)
    num_days=(calendar.monthrange(year,month)[1])
    end_date=date(int(y),int(m),num_days)
    e = Expenditure.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    k = Payment.objects.filter(date__gte=first_day_of_current_month,date__lte=end_date)
    u =User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True)
    for i in u:
        salary += i.salary
    expense = salary
    for o in e:
        expense += o.amount
    for j in k:
        amounts += j.amountrecieved
    if request.method == 'POST':
        date_of_expense=request.POST.get('date')
        describe=request.POST.get('description')
        amounts=request.POST.get('amount')
        Expenditure.objects.create(date=date_of_expense,description=describe,amount=amounts)
        return redirect('monthly')
    return render(request,'monthly.html',{'logos':logos,'e':e,'searchDate':b,'amounts':amounts,'salary':salary,'expense':expense,'b':b})

@my_login_required
def delete_expense(request,expenseid):
    exp=Expenditure.objects.filter(id=expenseid)
    exp.delete()
    return redirect('monthly')
########### engagement ##########
@my_login_required
@allowed_users(allowed_roles=['Admin'])
def listEngagements(request):
    active=Engagement.objects.filter(status="Active")
    completed=Engagement.objects.filter(status="Completed")
    datas_list=completed
    data=Engagement.objects.filter(status="Active")
    # if not data:
    #     messages.info(request,'No Records Found')
    
    User.objects.filter().order_by('status')
    query = request.GET.get('q')
    if query:
        data = Engagement.objects.filter(engagementName__contains=query,status="Active")
        # if not data:
        #     messages.info(request,"No Records Found")
    paginator = Paginator(datas_list, 20) # 6 datas per page
    page = request.GET.get('page',1)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)

    paginator = Paginator(data, 20)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    return render(request,'engagement.html',{'logos':logos,'datas':datas,'data':data,'messeges':messages})
def create_engagement(request):
    if request.method == 'POST':
        engagementName = request.POST.get('engagement')
        email = request.POST.get('email')
        status = 'Active'
        eng=Engagement.objects.filter(engagementName=engagementName).exists()
        if not eng :
            c = Engagement.objects.create(engagementName=engagementName,email=email,status=status)
    return redirect('listEngagements')

@my_login_required
def editengagement(request):
    if request.method=='POST':
        engagementName = request.POST.get('engagement')
        email = request.POST.get('email')
        status = request.POST.get('status')
        engagement_id=request.POST.get('engagement_id')
        engagement_id=request.POST.get('engagement_id') 
        k=Engagement.objects.filter(id=engagement_id)
        k.update(engagementName=engagementName,email=email,status=status)
        eng=Engagement.objects.filter(engagementName__contains=engagementName)
        # q=Engagement.objects.filter(engagementName=engagementName)
        a=list(Client.objects.filter(Parent__in=eng,status="Active"))
        b=list(User.objects.filter(engagement__in=eng,status="Active"))
        nott= a + b
        print('=================',nott)
        # print(b)
        if status=="Completed":
            if(not(nott)) :
                k.update(engagementName=engagementName,email=email,status="Completed")
            else:
                messages.error(request,"Status of the Account Manager or Project is Active")
                k.update(engagementName=engagementName,email=email,status="Active")
        else:
            k.update(engagementName=engagementName,email=email)
        return redirect('listEngagements')

@my_login_required
@allowed_users(allowed_roles=['Admin'])
def listconsultant(request):
    engagement =Engagement.objects.all()
    activeusers=list(User.objects.filter(status='Inactive',role="Consultant").exclude(is_superuser=True)) 
    data=list(User.objects.filter(status='Active',role="Consultant").exclude(is_superuser=True))
    if not data:
        messages.info(request,'No Records Found')
    datas_list=activeusers 
    query = request.GET.get('q')
    if query:
        data = User.objects.filter(first_name__contains=query,status="Active",role="Consultant").exclude(role="Employee")
        if not data:
            messages.info(request,"No Records Found")
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 20)
        
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)

    paginator = Paginator(data, 20)
        
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request,'consultant.html',{'logos':logos,'datas': datas,'data':data,'engagement':engagement,'messeges':messages}) 

@csrf_exempt
@my_login_required
def addConsultant(request):
    datas=User.objects.filter(role="Consultant").order_by('-id')
    engagement =Engagement.objects.all()
    if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        billing=request.POST.get('billing')
        status='Active'
        role=request.POST.get('role')
        if User.objects.filter(phone=phone).exists():         
            messages.error(request, 'Phone Number already exists!')
            return redirect('listconsultant')
        if User.objects.filter(email=email).exists():         
            messages.error(request, 'Email Id already exists!')
            return redirect('listconsultant')
            # messege="Phone Number already exists"
            # k={'status':status,"role":role,"name":name,'email':email,'billing':billing,}
            # return render(request,'consultant.html',{'messege':messege,'userdetails':k,'datas':datas,'engagement':engagement})
        otp=random.randint(000000,999999)
        user=User.objects.create_user(status=status,role="Consultant",first_name=name,phone=phone,email=email,billing=billing,otp=otp,password=str(otp))
        engagements=request.POST.getlist('engagement')
        engagements=Engagement.objects.filter(id__in=engagements).values_list('id',flat=True)
        user.engagement.set(engagements)
        user.save()
        
        #officemail ="operations@cydeztechnologies.com"
        subject, from_email, to = 'Created Successfully', settings.EMAIL_HOST_USER,user.email

        html_content = render_to_string('consultantemail.html', {'user': user,'otp':otp} )
        # render with dynamic value
        #html_content = render_to_string('consultantemail.html', {'user': user,'otp':otp} )
        text_content = strip_tags(html_content) 
        # Strip the html tag. So people can see the pure text at least.
        # create the email, and attach the HTML version as well.-       
        msg = EmailMultiAlternatives(subject, text_content,from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        #subject='Cydez Technologies'
        message='Hi your username is {0}  and password is {1} and please login "http://sf.cydeztechnologies.com/" Thank you'.format(user.phone,user.otp)
        #username=user.phone
        #password=user.otp
        #email_from=settings.EMAIL_HOST_USER
        #officemail ="operations@cydeztechnologies.com"
        #recipient_list=[user.email,user.officemail]
        #send_mail(subject,message,email_from,recipient_list)
        messages.success(request, 'Created Successfully')
        return redirect('listconsultant')
    return render(request,'consultant.html',{'logos':logos,'datas': datas,'engagement':engagement})

@my_login_required
def editConsultant(request): 
   if request.method=='POST':
        name=request.POST.get('name')
        phone=request.POST.get('phone')
        email=request.POST.get('email')
        user_id=request.POST.get('userId')
        status=request.POST.get('status')

        engagements=request.POST.getlist('engagement')
        if engagements:
            engagements=Engagement.objects.filter(engagementName__in=engagements).values_list('id',flat=True)
            pd=User.objects.get(id=user_id)
            k=pd.engagement.all().values_list('id',flat=True)
            if k:
                pd.engagement.remove(*k)
            pd.engagement.set(engagements)
            pd.save()

        j=User.objects.filter(id=user_id)
        user_phone=User.objects.filter(phone=phone)
        uw=User.objects.filter(email=email)
        
        if user_phone and user_phone.exists() and user_phone.first().id!=j.first().id:
            messages.error(request, 'Phone Number already exists')
        elif uw and uw.exists() and uw.first().id!=j.first().id:
            messages.error(request, 'Email Id already exists')
        else:
            j.update(status=status,first_name=name,phone=phone,email=email)
            messages.success(request,'Updated Successfully!')
        return redirect('listconsultant')

@csrf_exempt
@my_login_required
@allowed_users(allowed_roles=['Consultant'])
def consultatntbilling(request,userid):
    r=User.objects.filter(id=userid)
    if r:
        engagement=list(r[0].engagement.all().values_list('id',flat=True) )
        l=Client.objects.filter(status="Active",Parent__in=engagement)
        emp=list(Task.objects.filter(project__in=l).values_list('employee_id',flat=True))
        k=User.objects.filter(id__in=emp).exclude(is_superuser=True)
        df=pd.DataFrame(Task.objects.filter(project__in=l).values())
        datas_list=Task.objects.filter(project__in=l)
        if request.method == 'POST':
            task_id = request.POST.get('taskId')
            status = int(request.POST.get('status'))
            bs = Task.objects.get(id=task_id)
            if status:
                if not bs.status:
                    bs.status = True
                    if bs.amount:
                        project = bs.project
                        if hasattr(project,'estimate'):
                            estimate = bs.project.estimate
                            estimate.billed_amount += bs.amount
                            engagement=bs.project.Parent
                            engagements=Engagement.objects.get(engagementName=engagement)
                            engagements.engagementValue += bs.amount
                            engagements.save()
                            user=User.objects.filter(engagement=engagement,status="Active").values()
                            for i in user:
                                k=i['id']
                                user=User.objects.get(id=k)
                                user.value += bs.amount
                                user.save()
                            estimate.save()
                    
            else:
                if bs.status == True:
                    bs.status = False
                    if bs.amount:
                        project = bs.project
                        if hasattr(project,'estimate'):
                            estimate = bs.project.estimate
                            estimate.billed_amount -= bs.amount
                            engagement=bs.project.Parent
                            engagements=Engagement.objects.get(engagementName=engagement)
                            engagements.engagementValue -= bs.amount
                            engagements.save()
                            user=User.objects.filter(engagement=engagement,status="Active").values()
                            for i in user:
                                k=i['id']
                                user=User.objects.get(id=k)
                                user.value -= bs.amount
                                user.save()
                            estimate.save()             
            bs.save()
            return JsonResponse({"status":True,"message":"success","button":status})
        l=Client.objects.filter(status="Active",Parent__in=engagement)
        k=User.objects.filter(status="Active",id__in=emp).exclude(is_superuser=True)
        datas_list=Task.objects.filter(project__in=l)
        s = request.GET.get('status')
        p=request.GET.get('name')
        e=request.GET.get('employee')
        a=request.GET.get('start_date')
        b=request.GET.get('end_date')
        today=date.today()
        path=os.path.join(BASE_DIR,'media/csvfolder/billings.xlsx')
        loop_count = 1
        writer = pd.ExcelWriter(path, engine='xlsxwriter')
        per_total = 0
        total_amount = 0
        employees={}
        search_variables = {}
        
        if s:   
            search_variables['status__contains'] = s
        if p:
            search_variables['project__id'] = p
        else:
            search_variables['project__in'] = l
        if e:
            search_variables['employee__id'] = e
        if a:        
            search_variables['date__lte'] = a
        if b:
            search_variables['date__gte'] = b
        for i in User.objects.filter(status="Active",id__in=emp).exclude(is_superuser=True):
            task = i.task_set.filter(**search_variables)
            task1 =task.values('status','amount')
            task = task.values('employee__first_name','date','day','project__clientName','taskdetails','hours','amount')
            if task:
                index=0
                for j in task1:
                    if j['status'] == True:
                        per_total += j['amount']
                    else:
                        task[index]['amount']=0
                    o=per_total
                    index+=1
                per_total=0
                
                df=pd.DataFrame(task)
                df2 = {'employee__first_name':'','date':'','day':'','project__clientName':'','taskdetails':'','hours':'Total','amount':o}
                df = df.append(df2, ignore_index = True)
                df.columns=['Employee','Date','Day','Project','Taskdetails','Hours','Amount']
                df['Date'] = pd.to_datetime(df.Date)
                df['Date'] = df['Date'].dt.strftime('%d-%m-%Y')
                
                loop_count += 1
                df.to_excel(writer, sheet_name='Sheet' + str(loop_count),index=False)
                ### for adjust width ###
                worksheet=writer.sheets['Sheet' + str(loop_count)]
                for idx,col in enumerate(df):
                    series=df[col]
                    max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
                    worksheet.set_column(idx,idx,max_len)
                employees[i.get_full_name()]=[o]
                total_amount += o
        employees['Total']=[total_amount]
        new_df = pd.DataFrame(employees)
        new_df.to_excel(writer, sheet_name='Sheet1',index=False)
        writer.save()
        if  search_variables == {}:
            datas_list=Task.objects.filter(project__in=l).order_by('-id')
        else:
            datas_list=Task.objects.filter(**search_variables).order_by('-id')
        if not datas_list:
            messages.info(request,"No Records Found")
        page = request.GET.get('page', 1)    
        paginator = Paginator(datas_list, 20)
        try:
            datas = paginator.page(page)
        except PageNotAnInteger:
            datas = paginator.page(1)
        except EmptyPage:
            datas = paginator.page(paginator.num_pages)
        if p:
            p=int(p)
        if e:
            e=int(e)
        return render(request,"consultantsearch.html",{'logos':logos,'datas':datas,'l':l,'k':k,'s':s,'p':p,'e':e,'startdate':a,'enddate':b,'search_variables':search_variables,'userid':userid,'messeges':messages})
    else:
        return render(request, '404.html')

@csrf_exempt
@my_login_required
def approve_allConsultatnt(request,userid):
    today = date.today()
    d=today.day
    dd = today - timedelta(days = 2)
    r=User.objects.filter(id=userid)
    engagement=list(r[0].engagement.all().values_list('id',flat=True) )
    l=Client.objects.filter(status="Active",Parent__in=engagement)
    tk=Task.objects.filter(project__in=l,status=None).filter(date__gte= dd)
    if tk:
        for i in tk:
            i.status = True
            project = i.project
            if hasattr(project,'estimate'):
                estimate = i.project.estimate
                estimate.billed_amount += i.amount
                estimate.save()
            i.save()
    return redirect('consultatntbilling',userid=userid)

@my_login_required
def pendingc(request,userid):
    userid=userid
    today = date.today()
    d=today.strftime("%A")
    dd = today - timedelta(days = 2) 
    ddd=dd.strftime("%A")
    r=User.objects.filter(id=userid)
    engagement=list(r[0].engagement.all().values_list('id',flat=True) )
    l=Client.objects.filter(status="Active",Parent__in=engagement)
    emp=list(Task.objects.filter(project__in=l).values_list('employee_id',flat=True))
    u=list(User.objects.filter(id__in=emp).exclude(is_superuser=True).values_list("id",flat=True))
    k = list(Task.objects.filter(date__lte=today,date__gte= dd,project__in=l).values_list("employee",flat=True))
    res = list(set(u)^set(k))
    datas_list = User.objects.filter(id__in=res) 
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    if request.method=='POST':
        s=request.POST.get('search')
        datas_list=User.objects.filter(first_name__contains=s)
        page = request.GET.get('page', 1)
        paginator = Paginator(datas_list, 20)
        try:
            datas = paginator.page(page)
        except PageNotAnInteger:
            datas = paginator.page(1)
        except EmptyPage:
            datas = paginator.page(paginator.num_pages)
        return render(request,"pending_taskc.html",{'userid':userid,'datas':datas,'k':k,'datas_list':datas_list,'s':s,'today':today,'dd':dd,'ddd':ddd,'d':d})
    return render(request,'pending_taskc.html',{'logos':logos,'userid':userid,'datas':datas,'k':k,'datas_list':datas_list,'today':today,'dd':dd,'ddd':ddd,'d':d})

@my_login_required
@allowed_users(allowed_roles=['Consultant'])
def estimateconsultant(request,userid):
    userid=userid
    r=User.objects.filter(id=userid)
    engagement=list(r[0].engagement.all().values_list('id',flat=True) )
    l=list(Client.objects.filter(status="Completed",Parent__in=engagement).values_list('id',flat=True))
    m=list(Client.objects.filter(status="Active",Parent__in=engagement).values_list('id',flat=True))
    data1=list(Estimate.objects.filter(projectname__in=m).values_list('id',flat=True))
    data=Estimate.objects.filter(projectname__in=m)
    if not data:
        messages.info(request,'No Records Found')
    datas_list1=list(Estimate.objects.filter(projectname__in=l).values_list('id',flat=True))
    datas_list=Estimate.objects.filter(projectname__in=l)
    page = request.GET.get('page', 1)
    paginator = Paginator(datas_list, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)


    paginator = Paginator(data, 20)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)       
### for search project ###
    if request.method=='POST':
        s=request.POST.get('search')
        if s:
            data=Estimate.objects.filter(projectname__clientName__contains=s,projectname__status='Active',projectname__in=m)
            if not data:
                messages.info(request,"No Records Found")
            page = request.GET.get('page', 1)
            paginator = Paginator(data, 20)
            try:
                data = paginator.page(page)
            except PageNotAnInteger:
                data = paginator.page(1)
            except EmptyPage:
                data = paginator.page(paginator.num_pages)
    
        #     result =  any(elem in datas_list2  for elem in data1)
        #     if result:
        #         data=Estimate.objects.filter(projectname__clientName__startswith=s)
        #         page = request.GET.get('page', 1)
        #         paginator = Paginator(data, 20)
        #         try:
        #             data = paginator.page(page)
        #         except PageNotAnInteger:
        #             data = paginator.page(1)
        #         except EmptyPage:
        #             data = paginator.page(paginator.num_pages)


        #         paginator = Paginator(data, 20)
        #         try:
        #             data = paginator.page(page)
        #         except PageNotAnInteger:
        #             data = paginator.page(1)
        #         except EmptyPage:
        #             data = paginator.page(paginator.num_pages)
        #         return render(request,'estimatec.html',{'userid':userid,'datas':datas,'s':s,'data':data})
        #     else:
        #         j=[]
        #         data=Estimate.objects.filter(projectname__clientName__in=j)
        #         page = request.GET.get('page', 1)
        #         paginator = Paginator(data, 20)
        #         try:
        #             data = paginator.page(page)
        #         except PageNotAnInteger:
        #             data = paginator.page(1)
        #         except EmptyPage:
        #             data = paginator.page(paginator.num_pages)
        #         return render(request,'estimatec.html',{'userid':userid,'datas':datas,'data':data,'s':s,'messeges':messages})
        # else:
        #     data=Estimate.objects.filter(id__in=m)
        #     page = request.GET.get('page', 1)
        #     paginator = Paginator(data, 20)
        #     try:
        #         data = paginator.page(page)
        #     except PageNotAnInteger:
        #         data = paginator.page(1)
        #     except EmptyPage:
        #         data = paginator.page(paginator.num_pages)  
            return render(request,'estimatec.html',{'userid':userid,'datas':datas,'data':data,'l':l,'m':m})
    return render(request,'estimatec.html',{'userid':userid,'datas':datas,'data':data,'l':l,'m':m,'logos':logos})

@my_login_required
def estimation(request,userid):
    estimations = Estimation.objects.filter(consultantid=userid)
    page = request.GET.get('page', 1)
    paginator = Paginator(estimations, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'est.html',{'userid':userid,'datas':datas,'logos':logos})

@my_login_required
def generateEstimate(request,userid):
    userid=userid
    if request.method == 'POST':
        reportName=request.POST.get('report')
        reportNumber=request.POST.get('count')
        customerName=request.POST.get('customer')
        date=request.POST.get('date')
        currency=request.POST.get('currency')
        expireyDate=request.POST.get('expiresDate')
        memo=request.POST.get('memo')
        consultantid=User.objects.get(id=userid)
        data=Estimation.objects.create(consultantid=consultantid,reportName=reportName,reportNumber=reportNumber,customerName=customerName,date=date,currency=currency,expireyDate=expireyDate,memo=memo)
        estimationid = Estimation.objects.get(id=data.id)
        product=request.POST.getlist('combination_id1[]')
        description=request.POST.getlist('combination_id2[]')
        quantity=request.POST.getlist('combination_id3[]')
        price=request.POST.getlist('combination_id4[]')
        tax=request.POST.getlist('combination_id5[]')
        amounts=0
        amountss=[]
        L1=[]
        L1.append(product)
        L1.append(description)
        L1.append(quantity)
        L1.append(price)
        L1.append(tax)
        N = len(L1)
        j=0
        while j < len(product):
            res = [i[j] for i in L1[ : N]]
            pr=res[0]
            description=res[1]
            quan=res[2]
            prices=res[3]
            tax=res[4]
            tax = int(tax) /100
            amount= float(prices) * float(tax)
            amount=int(amount)
            amounts +=   amount
            amountss.append(amount)
            d=Productdetails.objects.create(estimationid=estimationid,product=pr,description=description,quantity=quan,price=prices,tax=tax,amount=amount)
            j += 1
        # write to pdf 
        a_dict = dict()
        i=0
        amt=[str(x) for x in amountss]
        while i < len(product):
            if 'product' in a_dict:
                a_dict['product'].append(product[i])
            else:
                a_dict['product'] = [product[i]]
            if 'quantity' in a_dict:
                a_dict['quantity'].append(quantity[i])
            else:
                a_dict['quantity'] = [quantity[i]]
            if 'price' in a_dict:
                a_dict['price'].append(price[i])
            else:
                a_dict['price'] = [price[i]]
            if 'amountss' in a_dict:
                amountss=str(amountss)
                a_dict['amountss'].append(amt[i])
            else:
                amountss=str(amountss)
                a_dict['amountss'] = [amt[i]]
            i +=1
        k=dict()
        k['reportNumber']=reportNumber
        k['date']=date_convert(date)
        k['expireyDate']=date_convert(expireyDate)
        amounts=str(amounts)
        k['amounts'] = amounts
        s=printoncertificate(data.id,k,a_dict)
        topdf(s.id)

        estimations = Productdetails.objects.filter(estimationid=estimationid)
        return render(request,'invoice.html',{'id':data.id,'certificateImage':s.image.url,'userid':userid,'data':estimations,'customerName':customerName,'reportNumber':reportNumber,'date':date,'expireyDate':expireyDate})
    return render(request,'estimatebilling.html',{'userid':userid,'logos':logos})

@my_login_required
def invoice(request,id):
    userid=id
    # estimationid = Estimation.objects.filter(id=id)
    # estimations = Productdetails.objects.filter(estimationid__id=estimationid[0].id)
    s = Estimation.objects.get(id=id)
    return render(request,'invoices.html',{'id':s.id,'certificateImage':s.image.url})

@my_login_required
def GeneratePDF(request,id):
    s = Estimation.objects.get(id=id)
    path=s.pdf.path
    content_type = mimetypes.guess_type(path)
    filename=s.filename('pdf')   
   
    dwnfile=open(path,'rb')
    response= HttpResponse(dwnfile,content_type=content_type[0]) 
    response['Content-Disposition'] = "attachment; filename=%s"%filename

    return response
@my_login_required
def estimation_admin(request):
    estimations = Estimation.objects.all()
    query = request.GET.get('q')
    if request.method=='POST':
        query=request.POST.get('q')
        if query:
            datas_list = User.objects.filter(first_name__contains=query)
            if not datas_list:
                messages.info(request,"No Records Found")
    page = request.GET.get('page', 1)
    paginator = Paginator(estimations, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    if paginator.num_pages:
        messages.info(request,"No Records Found")   
    return render(request,'esti.html',{'datas':datas,'messege':message,'logos':logos})

## For 404 error
def error_404(request, exception):
    return render(request, '404.html' ,{'logos':logos})


# def Settings(request):
#     data=Departments.objects.all()
#     return render(request,'list_departments.html',{ 'data':data,}) 

@my_login_required 
@allowed_users(allowed_roles=['Admin']) 
def Settings(request):
    data=Departments.objects.all()
    
    datas_list=Departments.objects.all()
    if not datas_list:
        messages.info(request,"No Records Found")
    
    query = request.GET.get('q')
    if query:
        data = Departments.objects.filter(Dname__contains=query)
        if not data:
            messages.info(request,"No Records Found")
    paginator = Paginator(datas_list, 20) # 6 datas per page
    page = request.GET.get('page',1)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)

    paginator = Paginator(data, 20)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request,'list_departments.html',{'datas':datas,'data':data,'messeges':messages,'logos':logos})

@my_login_required
def create_department(request):
    if request.method == 'POST':
        Dname = request.POST.get('Dname')
        description = request.POST.get('Description')
        z=Departments.objects.create(Dname=Dname,description=description)
        return redirect('setting')
@my_login_required
def del_Department(request,departmentid):
    c=Departments.objects.get(id=departmentid)
    c.delete()
    return redirect('setting')

def editDepartment(request):
    if request.method=='POST':
        Dname=request.POST.get('Dname')
        description = request.POST.get('Description')
        departmentid=request.POST.get('departmentid')
        d=Departments.objects.filter(id=departmentid)
        d.update(Dname=Dname,description=description)
        return redirect('setting')

# def list_designation(request):
#     ob=Designations.objects.all()
#     return render(request,'Designation.html',{ 'ob':ob,}) 
@allowed_users(allowed_roles=['Admin'])
def list_designation(request):
    ob=Designations.objects.all()
    datas_list=Designations.objects.all()
    if not datas_list:
        messages.info(request,'No Records Found')
    
    query = request.GET.get('q')
    if query:
        data = Designations.objects.filter(Designation__contains=query)
        if not data:
            messages.info(request,"No Records Found")
    paginator = Paginator(datas_list, 20) # 6 datas per page
    page = request.GET.get('page',1)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)

    paginator = Paginator(ob, 20)
    try:
        ob = paginator.page(page)
    except PageNotAnInteger:
        ob = paginator.page(1)
    except EmptyPage:
        ob = paginator.page(paginator.num_pages)

    return render(request,'Designation.html',{'datas':datas,'ob':ob,'messeges':messages,'logos':logos})

def del_Designation(request,designationid):
    cd=Designations.objects.get(id=designationid)
    cd.delete()
    return redirect('list_Designations')

def create_designation(request):
    if request.method == 'POST':
        Designation= request.POST.get('Designation')
        y=Designations.objects.create(Designation=Designation)
        return redirect('list_Designations')
    
def edit_designation(request):
    if request.method=='POST':
        Designation= request.POST.get('Designation')
        designationid=request.POST.get('designationid')
        k1=Designations.objects.filter(id=designationid)
        k1.update(Designation=Designation)
        return redirect('list_Designations')

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def emp_knowledgecenter(request):
    datas=Knowledge.objects.all().prefetch_related('knowledgelink_set')
    from django.db.models import Value
    datas.annotate(links=Value(''))
    query=request.GET.get('q') 
    if query:
        datas1 = list(datas.filter(department__Dname__contains=query))
        datas2 = list(datas.filter(description__contains=query))
        datas= list(set(datas1 + datas2))
        if not datas:
            messages.info(request,"No Records Found") 
    page = request.GET.get('page', 1)
    paginator = Paginator(datas, 20)
    try:
        datas = paginator.page(page) 
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
   
    for i in datas:
        i.links=list(i.knowledgelink_set.values_list('link',flat=True))
        i.attach=list(i.attachment_set.values_list('attach',flat=True))
    return render(request,'emp_knowledgecenter.html',{'datas': datas,'messeges':messages,'logos':logos})

@csrf_exempt
def knowledge_center(request):
    datas=User.objects.all()
    Des=Designations.objects.all()
    Dep=Departments.objects.all()
    att=Attachment.objects.all()
    
    if request.method=='POST':       
        description=request.POST.get('description')
        links=request.POST.getlist('links')  
        department =request.POST.get('department')
        p=Departments.objects.get(id=department)     
        knowledge=Knowledge.objects.create(description=description,department=p)    
        # arr=link.strip('][').split()
        attachments=request.FILES.getlist('attachments') 
        for f in attachments:
            d=Attachment.objects.create(attach=f,knowledges=knowledge)
            d.save()

        for link in links:
            if link and link.strip():
                d=KnowledgeLink.objects.create(link=link,knowledge=knowledge)
                d.save()
           
        return redirect('list_knowledge')
    return render(request,'knowledge.html',{'datas': datas,'Des':Des,'Dep':Dep,'att':att,'logos':logos})
@my_login_required
@allowed_users(allowed_roles=['Admin'])
def list_knowledge(request):
    Dep=Departments.objects.all()
    Des=Designations.objects.all()
    pk=[]
    datas=Knowledge.objects.all().prefetch_related('knowledgelink_set')
    from django.db.models import Value
    datas.annotate(links=Value(''))
    if not datas:
        messages.info(request,"No Records Found")
   
    query=request.GET.get('q') 
    if query:
        datas1 = list(datas.filter(department__Dname__contains=query))
        datas2 = list(datas.filter(description__contains=query))
        datas= list(set(datas1 + datas2))
        if not datas:
            messages.info(request,"No Records Found")   
    page = request.GET.get('page', 1)
    paginator = Paginator(datas, 20)
    try:
        datas = paginator.page(page) 
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    
    for i in datas:
        i.links=list(i.knowledgelink_set.values_list('link',flat=True))
   
        i.attach=list(i.attachment_set.values_list('attach',flat=True))

    return render(request,'knowledge.html',{'datas': datas,'messeges':messages,'Dep':Dep,'Des':Des,'logos':logos})
@my_login_required
def del_knowledge(request,knowledgeid):
    cd=Knowledge.objects.get(id=knowledgeid)
    cd.delete()
    return redirect('list_knowledge')

@my_login_required   
def editKnowledge(request):  
      
    if request.method=='POST':
        description=request.POST.get('description')   
        knowledgeid=request.POST.get('knowledgeid')        
        department =request.POST.get('department')
        p=Departments.objects.get(Dname=department)
        k=Knowledge.objects.filter(id=knowledgeid) 
        k.update(department=p,description=description) 
        attachments=request.FILES.getlist('attachments') 
        attachs=request.POST.getlist('attachs')       
        links=request.POST.getlist('links')     
        datas=Knowledge.objects.all().prefetch_related('knowledgelink_set')
        from django.db.models import Value
        datas.annotate(links=Value(''))
        # append file path with attachment
        for i in range(len(attachs)):
            attachs[i]="media/"+attachs[i]
        # delete existing attachment which are removed
        missing_Attachment_list=Attachment.objects.filter(knowledges__in=k).exclude(attach__in=attachs)
        missing_Attachment_list.delete()
        
        for f in attachments:
            d=Attachment.objects.create(attach=f,knowledges_id=knowledgeid)
            d.save()
        
        exiting_links=KnowledgeLink.objects.filter(knowledge_id=knowledgeid)
        exiting_links.delete()
        for link in links:
            if link and link.strip():
                d1=KnowledgeLink.objects.create(link=link,knowledge_id=knowledgeid)
                d1.save() 
        for i in datas:
            i.links=list(i.knowledgelink_set.values_list('link',flat=True))
   
            i.attach=list(i.attachment_set.values_list('attach',flat=True))           
        return redirect('list_knowledge')
    return render(request,'Knowledge.html',{'logos':logos})

@allowed_users(allowed_roles=['Admin'])
def add_logo(request):
    logos = Logo.objects.all()
    print("aaaaaaa", logos)
    if request.method == 'POST':
        # email = request.POST.get('email')   
        add_logo = request.FILES.get('add_logo')
        f1 = FileSystemStorage()
        if add_logo :
            f2 = f1.save(add_logo.name,add_logo)
        d = Logo.objects.create(add_logo = add_logo)
        d.save()
    return render(request,'logo.html' , { 'logos': logos })

def del_logo(request,logo_id):
    l=Logo.objects.get(id=logo_id)
    l.delete()
    return redirect('add_logo')

@allowed_users(allowed_roles=['Admin'])
def add_email_configuration(request):

    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        module = request.POST.get('module')
        sname = request.POST.get('Sname')
        cname = request.POST.get('Cname')
        ab_co = request.POST.get('abt_company')
        address = request.POST.get('address')
        # logo = request.FILES["logo"]
        logo=request.FILES['logo']
        f=FileSystemStorage()
        fn=f.save(logo.name,logo)
        is_on = request.POST.get('switch')
        print(is_on)

        if is_on == 1:

            EmailConfig.objects.create(
                emailid=email, app_password=password, module=module, sender_name=sname, company_name=cname, about_company=ab_co, address=address, logo=fn, is_on=1)

            return redirect('listec')
        else:
            EmailConfig.objects.create(
                emailid=email, app_password=password, module=module, sender_name=sname, company_name=cname, about_company=ab_co, address=address, is_on=0)
            return redirect('listec')
        return redirect('listec')

    return render(request, 'EmailConfig.html',{ 'logos': logos })

@allowed_users(allowed_roles=['Admin'])
def list_email_configuration(request):
    
    datas = EmailConfig.objects.all()

    query = request.GET.get('search' , '')
    page = request.GET.get('page', 1)

    if query:
        datas_list1 = EmailConfig.objects.filter(emailid__contains=query)
        datas_list2 = EmailConfig.objects.filter(module__contains = query)

        datas = datas_list1 | datas_list2

    page = request.GET.get('page', 1)
    paginator = Paginator(datas, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request, 'EmailConfig.html', {'datas': datas})


def edit_email_configuration(request):

    datas = EmailConfig.objects.all()
    if request.method == 'POST':

        email = request.POST.get('email')
        module = request.POST.get('module')
        password = request.POST.get('password')
        sname = request.POST.get('Sname')
        cname = request.POST.get('Cname')
        ab_co = request.POST.get('abt_company')
        address = request.POST.get('address')
        is_on = request.POST.get('switch')
        ecId = request.POST.get('EcId')
        

        data = EmailConfig.objects.filter(id=ecId)

        if data:
            if is_on == '1':
                data.update(emailid=email, module=module, app_password=password, sender_name=sname,
                            company_name=cname, about_company=ab_co, address=address, is_on=1)

                return redirect('listec')
            else:
                data.update(emailid=email, module=module, app_password=password, sender_name=sname,
                            company_name=cname, about_company=ab_co, address=address, is_on=0)

                return redirect('listec')
        else:

            return JsonResponse('Data not found')

    page = request.GET.get('page', 1)
    paginator = Paginator(datas, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request, 'EmailConfig.html', {'datas': datas})


def del_email_configuration(request, uid):

    EmailConfig.objects.get(id=uid).delete()

    return redirect('listec')

@my_login_required
@allowed_users(allowed_roles=['Admin'])
def taskschedular(request):



    k1=list(User.objects.filter(status="Active" , role="Employee").exclude(role="Consultant"))
    k2=list(User.objects.filter(status="Active" , role="Admin"))
    k= k1 + k2
    datas=TaskSchedular.objects.all().order_by('-end_date')
    # datas_list=My_task.objects.filter(created_by=request.user).order_by("-end_date")
    e=request.GET.get('users')  
    # c=request.GET.get('usr')   
    a=request.GET.get('start_date') 
    b=request.GET.get('end_date') 
    s=request.GET.get('status')

    search_variables = {}
   
    if e : 
        search_variables['asignee__contains']=e  
    # if c : 
    #     search_variables['created_by__contains']=c   
    if s:
        search_variables['status__contains']=s   
    if a: 
        search_variables['end_date__gte']=a
    if b: 
        search_variables['end_date__lte']=b
    
    datas=TaskSchedular.objects.filter(**search_variables).order_by('-end_date') 
    if not datas:
        messages.info(request,"No Records Found")    
   
    paginator = Paginator(datas, 20) # 6 datas per page
    page = request.GET.get('page',1) 
    try:
        datas = paginator.page(page)
    except PageNotAnInteger: 
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    return render(request,'taskschedular.html',{'logos':logos,'k':k,'datas':datas}) 


@my_login_required
def add_taskschedular(request):
    # user=request.user
    if request.method=='POST':
        title=request.POST.get('title')
        description=request.POST.get('description')
        end_date=request.POST.get('date')
        start_date=request.POST.get('startdate')
        asignee=request.POST.get('asignee') 
        comments=request.POST.get('comments')
        created_by=request.POST.get('created') 
        interval = request.POST.get('interval')        
        try:
            attachment=request.FILES['attachment']
            if not attachment:
                attachment=None
        except:
            attachment=None
        f1=FileSystemStorage()
        if attachment:
         f2=f1.save(attachment.name,attachment)    
        TaskSchedular.objects.create(title=title,description=description,end_date=end_date,start_date=start_date,asignee=asignee,status="New", created_by=created_by ,attachment=attachment, interval=interval,comments=comments)
        return redirect('taskschedular') 
    return render(request,'taskschedular.html')

@my_login_required
def edit_taskschedular(request):
    if request.method=='POST':
        title=request.POST.get('title')
        description=request.POST.get('description')
        start_date=request.POST.get('startdate')
        end_date=request.POST.get('date')
        interval=request.POST.get('interval')
        comments=request.POST.get('comments')
        asignee=request.POST.get('asignee') 
        status=request.POST.get('status')
          
        try:
            attachment=request.FILES['attachment']
            if not attachment:
                attachment=None
        except:
            attachment=None
        f1=FileSystemStorage()
        if attachment:
         f2=f1.save(attachment.name,attachment)   
        mytaskid=request.POST.get('mytaskid') 

        k=TaskSchedular.objects.filter(id=mytaskid)  
        k.update(title=title,description=description,start_date=start_date,end_date=end_date,asignee=asignee,status=status, interval=interval,comments=comments)
        if attachment:
            k.update(attachment=attachment)
        return redirect('taskschedular') 
    return render(request,'taskschedular.html')


def auto_taskschedular(): 
    print("helooddddddddddddddddddddddddddddddddddddddddddddddddddddd")
    schedular_datas = TaskSchedular.objects.all()
    today = date.today() 

    for i in schedular_datas:
        
        title=i.title
        description=i.description
        startdate = i.start_date
        d=datetime.strptime(startdate,"%Y-%m-%d")
        end_date=i.end_date
        asignee=i.asignee
        created_by=i.created_by
        attachment=i.attachment
        comments=i.comments
        interval = i.interval
        e=int(interval)

        scheduled_date = (d + timedelta(days = e ))
        k=scheduled_date.date()==today

        if k :
            TaskSchedular.objects.create(title=title,description=description,end_date=end_date,start_date=startdate,asignee=asignee,status="New", created_by=created_by ,attachment=attachment, interval=interval,comments=comments)
        else:
            print("No")

        print("dddddddd",scheduled_date.date())
        print("today",today)
        

@my_login_required
def delete_taskschedular(request,my_taskschedularid): 
    k=TaskSchedular.objects.get(id=my_taskschedularid)
    k.delete() 
    return redirect('taskschedular')

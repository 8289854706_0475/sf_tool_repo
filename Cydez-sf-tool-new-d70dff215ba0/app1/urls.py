from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('',views.loginf,name='loginf'),
    path('userlogout',views.userlogout,name='userlogout'),
    # Not require any more
    # path('adminlogin',views.adminlogin,name='adminlogin'),
    path('userlogout',views.userlogout,name='userlogout'),
    path('home',views.home,name='home'),
    path('dashboard',views.dashboard,name='dashboard'),
    path('forgotpassword',views.forgotPassword,name='forgotpassword'),
    path('resetpassword/<int:userid>',views.resetpassword,name='resetpassword'),
    path('resend_password',views.resend_password,name='resend_password'),
    ## user ##
    path('add',views.addUser,name='adduser'),
    path('listuser',views.listUser,name='listUser'),
    path('deleteUser<int:userid>',views.deleteUser,name='deleteUser'),
    # path('search',views.searchUser,name='searchUser'),
    # path('edit<int:userid>',views.editUser,name='editUser'),
    path('edit',views.editUser,name='editUser'),
    #path('savechanges',views.savechanges,name='savechanges'),
    ## estimate ##
    path('estimate',views.estimate,name='estimate'),
    path('listproject/',views.listProject,name='listProject'),
    path('deleteProject<int:projectid>',views.deleteProject,name='deleteProject'),
    # path('searchProject',views.searchProject,name='searchProject'),
    path('editestimate',views.editestimate,name='editestimate'),
    
    #### client #######
    path('clientlogin',views.clientlogin,name='clientlogin'),
    path('client_billing',views.client_billing,name='client_billing'),
    path('client_estimation',views.client_estimation,name='client_estimation'),
    path('client_accountmanager',views.client_accountmanager,name='client_accountmanager'),
    path('client_generatestimate',views.client_generatestimate,name='client_generatestimate'),
    path('create_client', views.create_client, name='create_client'),
    path('listclient', views.listclient, name='listclient'),
    path('searchclient', views.searchclient, name='searchclient'),
    path('editclient',views.editClient,name='editClient'),
    path('deleteclient/<int:id>', views.deleteclient, name='deleteclient'),

    ### Total billing ###
    path('Totalbilling',views.Totalbilling,name='Totalbilling'),
    path('approve_all',views.approve_all,name="approve_all"),
    path('pending',views.pending,name="pending"),
    # path('generate',views.generate,name='generate'),
    path('accept/<int:task_id>',views.accept,name='accept'),
    ### payment ##
    path('payment<int:clientid>',views.payment,name='payment'),
    path('addPayment',views.addPayment,name='addPayment'),
    path('deletePayment<int:paymentid>',views.deletePayment,name='deletePayment'),
    path('editPayment',views.editPayment,name='editPayment'),
    path('BillingPerfom<int:userid>',views.BillingPerfom,name="BillingPerfom"),
    path('Incentive',views.Incentive,name='Incentive'),
    path('monthly',views.monthly,name="monthly"),
    path('expenses',views.expenses,name="expenses"),
    path('delete_expense<int:expenseid>',views.delete_expense,name="delete_expense"),

    ### engagement ##
    path('listEngagements',views.listEngagements,name='listEngagements'),
    path('create_engagement',views.create_engagement,name='create_engagement'),
    path('editengagement',views.editengagement,name='editengagement'),
    ### consultant ###
    path('listConsultant',views.listconsultant,name='listconsultant'),
    path('addConsultant',views.addConsultant,name='addConsultant'),
    path('editConsultant',views.editConsultant,name='editConsultant'),
    path('consultant_billing/<int:userid>',views.consultatntbilling,name='consultatntbilling'),
    path('approve_allConsultant/<int:userid>',views.approve_allConsultatnt,name='approve_allConsultatnt'),
    path('pendingc/<int:userid>',views.pendingc,name='pendingc'),
    path('estimateconsultant/<int:userid>',views.estimateconsultant,name='estimateconsultant'),
    path('generateEstimate/<int:userid>',views.generateEstimate,name='generateEstimate'),
    path('estimation/<int:userid>',views.estimation,name='estimation'),
    path('invoice/<int:id>',views.invoice,name='invoice'),
    path('GeneratePDF/<int:id>',views.GeneratePDF,name='GeneratePDF'),
    path('estimation_admin',views.estimation_admin,name='estimation_admin'),
    path('task',views.task,name='task'),
    path('mytask',views.mytask,name='mytask'),
    path('add_task',views.add_task,name='add_task'),    
    path('edit_task',views.edit_task,name='edit_task'), 
    path('Dlt_task<int:my_taskid>',views.delete_Task,name='delete_Task'),
    path('task_employee',views.task_employee,name='task_employee'),
    path('employeetask_add',views.employeetask_add,name='employeetask_add'),
    path('edit_employeetask',views.edit_employeetask,name='edit_employeetask'),
    path('edit_employeemytask',views.edit_employeemytask,name='edit_employeemytask'),
    path('Dlt_emptask<int:my_taskid>',views.delete_empTask,name='delete_empTask'),
    path('Settings',views.Settings,name='setting'),
    path('add_department',views.create_department,name='add_department'),
    path('dlt_department<int:departmentid>',views.del_Department,name='del_Department'),
    path('edit_department',views.editDepartment,name='edit_department'),
    path('list_Designations',views.list_designation,name='list_Designations'),
    path('del_Designation<int:designationid>',views.del_Designation,name='del_Designation'),
    path('add_designation',views.create_designation,name='add_designation'),
    path('edit_designation',views.edit_designation,name='edit_designation'),
    path('emp_knowledgecenter',views.emp_knowledgecenter,name='emp_knowledgecenter'),
#knowledge
    path('knowledge_center',views.knowledge_center,name='knowledge_center'),
    path('list_knowledge', views.list_knowledge, name='list_knowledge'),
    path('del_knowledge<int:knowledgeid>', views.del_knowledge, name='del_knowledge'),
    path('editKnowledge',views.editKnowledge,name='editKnowledge'),
    path('add_logo',views.add_logo,name='add_logo'),
    path('del_logo<int:logo_id>', views.del_logo, name='del_logo'),

    #Task Schedular
    path('taskschedular',views.taskschedular,name='taskschedular'),
    path('add_taskschedular',views.add_taskschedular,name='add_taskschedular'),    
    path('edit_taskschedular',views.edit_taskschedular,name='edit_taskschedular'), 
    path('Dlt_taskschedular<int:my_taskschedularid>',views.delete_taskschedular,name='delete_taskschedular'),

     #Email Configuration Path
     path('listec' , views.list_email_configuration , name='listec'),
     path('addec' , views.add_email_configuration , name='addec'),
     path('deltec/<int:uid>' , views.del_email_configuration , name='delec'),
     path('edittec' , views.edit_email_configuration , name='editec'),
     
]+ static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

### 404 error 
handler404 = 'app1.views.error_404'
import imp
from django.http import HttpResponse
from django.shortcuts import redirect,render

def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('homes')
        else:
            return view_func(request, *args, **kwargs)
    return wrapper_func



def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            role=None
            print(request.user.role)
            if request.user.role in allowed_roles:
                return view_func(request, *args, **kwargs)
            else:
                return render(request, '404.html')
                return HttpResponse("You are not authorized to view this page")
        return wrapper_func
    return decorator
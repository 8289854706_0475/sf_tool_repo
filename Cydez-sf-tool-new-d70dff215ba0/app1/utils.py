from io import BytesIO
import time
from django.db import connection
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.loader import get_template
from xhtml2pdf import pisa
import mimetypes,datetime
import img2pdf
from io import BytesIO

from developers.models import Task
from .models import Estimation,Certificate, Logo, User
from PIL import Image,ImageDraw,ImageFont
from django.core.files.base import ContentFile
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from datetime import date
from datetime import timedelta
def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result= BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")),result)
    if not pdf.err:
        return HttpResponse(result.getvalue(),content_type='application/pdf')
    return None
def date_convert(date):
    date_f=date.split('-')
    for i in range(len(date_f)):
        date_f[i]=int(date_f[i])
    date_f=datetime.datetime(*date_f)
    date_f=date_f.strftime("%b %e, %Y")

    return date_f
def topdf(sid):
    s=Estimation.objects.get(id=sid)
    path=s.image.path

    pdf_bytes = img2pdf.convert(path)

    filename=s.customerName+'.pdf'
    s.pdf.save(filename,ContentFile(pdf_bytes),save=False)
    s.save()

    return None
def printoncertificate(sid,k,a_dict):
    #Get student from db
    s=Estimation.objects.get(id=sid)
    pname=s.customerName.upper()

    #Fetch certificate from db
    c=Certificate.objects.get(id=1)
    (x, y) = (c.xvalue, c.yvalue)
    #Pillow
    

    certificate_image = Image.open(c.image)
    
    draw = ImageDraw.Draw(certificate_image)

    #Hardcoded values starts
    font=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf',15)   
    font2=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 14)
    font3=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 14)
    font4=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 14)
    font5=ImageFont.truetype('static/fonts/Lato/Lato-Regular.ttf', 15)   
    
    if font.getsize(pname)[0]<1710:
        w=font.getsize(pname)[0]
        lindent=(1570-w)/2
        x+=lindent
    if font2.getsize(k['reportNumber'])[0]<1120:
        w=font2.getsize(k['reportNumber'])[0]
        indent=(1120-w)/2
        course_xvalue=1850+indent
    
    fillcolor = "black"
    fillcolor1 ="red"
    # draw.text((15,330), "oct5,2021", font=font,fill=fillcolor1)
    

    draw.text((25,210),pname,font=font,fill=fillcolor)
    draw.text((500,192),k['reportNumber'],font=font2,fill=fillcolor)
    draw.text((499,212),k['date'],font=font2,fill=fillcolor)
    draw.text((497,232),k['expireyDate'],font=font2,fill=fillcolor)
    draw.text((500,252),k['amounts'],font=font,fill=fillcolor)
    draw.text((500,532),k['amounts'],font=font5,fill=fillcolor)
    draw.text((500,567),k['amounts'],font=font,fill=fillcolor)
    dict2 = {}
    for k in a_dict:
        dict2[k] = len(a_dict[k])
    first_pair= dict2['product']
    i=0
    y=330
    while i< first_pair:
        draw.text((15,y),a_dict['product'][i],font=font3,fill=fillcolor)
        draw.text((330,y),a_dict['quantity'][i],font=font3,fill=fillcolor)
        draw.text((450,y),a_dict['price'][i],font=font4,fill=fillcolor)
        draw.text((550,y),a_dict['amountss'][i],font=font4,fill=fillcolor)
        y += 20
        i += 1
    #Hardcoding ends
    
    temp_buffer = BytesIO()
    certificate_image.save(temp_buffer, certificate_image.format)
    
    certificate_name=s.customerName
    certificate_name+='.'+certificate_image.format

    s.image.save(certificate_name,ContentFile(temp_buffer.getvalue()),save=False)
    s.save()

    return s

logos=Logo.objects.all()

def shopUserHome(request):
    if not request.user.is_authenticated:
        return render(request,'login.html',{'logos':logos})
    else:
        role=request.user.role
        if role == "Employee":
            return redirect('listtask')
        elif role == "Consultant":
            return redirect('consultatntbilling',userid=request.user.id)
        elif role == "Admin":
            return redirect('dashboard')
        else:
            return render(request,'login.html',{'logos':logos})
    

def getMissingUserDetails(query):
    today = date.today()
 
    day = 1
    users_data={}
    # Within 5 days
    while day < 6:
        check_date = today - timedelta(days = day)
        
        if check_date.strftime("%A") != 'Sunday':
            print("----------"+str(check_date))
            # List of Emp with no pending task
            emp_with_no_pending=Task.objects.filter(date= check_date,remainingHours=0).values_list("employee",flat=True)
            # for task in emp_with_no_pending:
            #     print(task)
            # print("********"+str(check_date))
            
            # Get list of Emp with pending task
            emp_with_pending_task=Task.objects.filter(date= check_date).exclude(employee_id__in = emp_with_no_pending)
            emp_with_pending=emp_with_pending_task.values_list("employee_id","employee__first_name","employee__email","remainingHours").distinct().order_by('-id')
           
            for emp in emp_with_pending:
                updateData(emp[0],str(emp[1])+'-'+str(emp[0]), query, users_data, 'missing_date', check_date,str(emp[3]))
                
            emp_with_pending=emp_with_pending_task.values_list("employee_id",flat=True).distinct()
           
            if query: 
                    absent_emp_lists=User.objects.filter(role="Employee",first_name__icontains=query,status="Active").exclude(date__gte=today).exclude(id__in = emp_with_no_pending).exclude(id__in = emp_with_pending).values_list("id","first_name").distinct()                    
            else:
                    absent_emp_lists=User.objects.filter(role="Employee",status="Active").exclude(date__gte=today).exclude(id__in = emp_with_no_pending).exclude(id__in = emp_with_pending).values_list("id","first_name").distinct() 
                
            for emp in absent_emp_lists:
                updateData(emp[0],str(emp[1])+'-'+str(emp[0]), query, users_data, 'absent_date', check_date,"8")
                

        day += 1 
    
    print(users_data)
    return users_data

def updateData(id,emp, query, users_data, key, date,remaingHour):
    from datetime import datetime
    if emp in users_data:
        user_dict = users_data.get(emp)
        creation_date=time.strptime(str(user_dict['creation_date'].date()), '%Y-%m-%d')
        check_date = time.strptime(str(date), '%Y-%m-%d')
       
        if creation_date <= check_date:
            if str(date) not in  user_dict[key]:
                user_dict[key] += str(date)+", "
                if key == 'missing_date':
                        user_dict['missing_hours'] += remaingHour+", "
            users_data[emp] = user_dict
    else:
        # role="Employee"
        today = date.today()
        if query: 
            datas_list=User.objects.filter(role="Employee",first_name__icontains=query,id=id,status="Active").exclude(date__gte= today)                     
        else:
            datas_list=User.objects.filter(role="Employee",id=id,status="Active").exclude(date__gte= today)
        datas_list=datas_list.first()
        if datas_list:
                     
            creation_date=time.strptime(str(datas_list.date.date()), '%Y-%m-%d')
            check_date = time.strptime(str(date), '%Y-%m-%d')
           
            if creation_date <= check_date:
                user_dict ={'email': datas_list.email,'first_name': datas_list.first_name,'phone': datas_list.phone,'missing_date':'','absent_date':'','creation_date':datas_list.date,'missing_hours':''} 
                if str(date) not in  user_dict[key]:
                    user_dict[key] += str(date)+", "
                    if key == 'missing_date':
                        user_dict['missing_hours'] += remaingHour+", "
                users_data[emp] = user_dict

    
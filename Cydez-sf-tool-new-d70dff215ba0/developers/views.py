import email
from lib2to3.pgen2.grammar import opmap
from tkinter import messagebox
from django.contrib.auth.decorators import login_required
from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from numpy import greater_equal
from rest_framework.response import Response 
from rest_framework.decorators import api_view
from app1.utils import getMissingUserDetails, updateData

from cydezproject.decorators import my_login_required
from .models import Task
import apscheduler.schedulers.background
from apscheduler.schedulers.background import BackgroundScheduler
from django.contrib import messages
from app1.models import Client, Departments, Engagement, Estimate, Logo, My_task,User,Incentives,Payment
from datetime import date,timedelta
import datetime
from pathlib import Path
from django.db.models import Sum
from django.utils.dateparse import parse_date
import calendar
from time import time
import json
from django.template.loader import render_to_string 
from django.utils.html import strip_tags
from django.conf import settings
from django.core.mail import send_mail
from django.core.mail  import EmailMultiAlternatives
from django.template.loader import get_template
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.contrib.auth import login,logout,authenticate
import pandas as pd
from app1.decorator import allowed_users

logos=Logo.objects.all()

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def create_task(request):
    today = date.today().strftime("%Y-%m-%d")
    user= request.user
    k = Task.objects.filter(date=today,employee=user)
    l= Client.objects.all()
    if request.method == 'POST':
        pro = request.POST.get('project')
        p = Client.objects.get(id=pro)
        add_date = request.POST.get("date")        
        dayname = datetime.datetime.strptime(add_date, "%Y-%m-%d")
        qq = dayname.strftime('%d-%m-%Y')
        user= request.user
        taskdetails = request.POST.get('taskdetails')
        hours = request.POST.get('hours')
        amount=user.amount*int(hours)
        today = date.today().strftime("%Y-%m-%d")
        day = dayname.strftime("%A")
        count=0
        remainingHours = int(8-(int(count)+int(hours)))
        if day == 'Sunday':
            return render (request,'listtask.html',{'k':k,'l':l,'rh':0,'msg':'sorry hours have exceded','sun':'Sorry Sunday is a Holiday' })
        if user.task_set.filter(date=add_date):
            k = Task.objects.filter(date=add_date,employee=user)
            for i in user.task_set.filter(date=add_date):
                count+= int(i.hours)
                remainingHours = int(8-(int(count)+int(hours)))
            if not int(count)<8 or not int(count)+int(hours)<=8:
                k = Task.objects.filter(date = add_date,employee = user)
                return render (request,'listtask.html',{'k':k,'l':l,'msg':'sorry hours is exceded','rh':0,'selectedDate':add_date })
        Task.objects.create(employee=user,date=add_date,day=day,project= p,taskdetails=taskdetails, hours=hours,amount=amount,remainingHours=remainingHours)
        request.session['search_date'] = add_date
        return redirect('listtask')
    return redirect('listtask')

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def listtask(request):
    user= request.user
    kk=User.objects.filter(date=request.user.date)
    u_d=user.date.date()
    # u_d=datetime.datetime.today().date()
    print('===========',u_d) 
    for i in kk:
        dates=i.date.date()
       
    d2 = datetime.datetime.today().date()
    print('===========',d2)

    difference=(d2-dates).days
    
    pk=[]        
    today = datetime.datetime.today()
    print('===========',today.date()) 
    numdays = difference
    for x in range (0, numdays):       
        date_ranges=d2 - datetime.timedelta(days = x)        
        if date_ranges.weekday() < 6:
            five_d=date_ranges.weekday() < 6
            pk.append(date_ranges)
    rv=[]
    rd=[]
    w=i.date                    
    e=i.status
    for z in pk:
       
        k=user.task_set.filter(date=z)
        count = 0
        
        if k:
            for i in k:
                check=i.status  
                q=d2 - datetime.timedelta(days = 6)  
                w1=i.date                                         
                if check == False and w1 > q :
                    w=i.date                    
                    e=i.status
                    print("****rejected date = ",w)
                    print("****status rejected = ",e)
                    rd.append(w.strftime("%m-%d-%Y"))
                count+= int(i.hours)
                remainingHour = int(8-int(count))
                # rm.append(z)
            rv.append(remainingHour)
             
        else:
            remainingHour = 8 
            # rm.append(z)
            rv.append(remainingHour)    
        
    if user.is_authenticated :
        message=False
        d=0
        if not 'search_date' in request.session:
            search_date = date.today().strftime("%Y-%m-%d")
        else:
            search_date = request.session.get('search_date')
            del request.session['search_date']
        m= search_date
        if request.method=='POST':
            search_date=request.POST.get('searchdate')
        search_date_f = datetime.datetime.strptime(search_date,"%Y-%m-%d").date()
        # datetime.datetime.strptime('January 11, 2010', '%B %d, %Y').strftime('%A')
        search_date_d = datetime.datetime.strptime(search_date,"%Y-%m-%d").strftime('%A')
        date_range = date.today()-timedelta(days=5)
        date_end = date.today()+timedelta(days=1)
        if search_date_f < date_range or search_date_f > date.today():
            message=" "
            d="1"
        k=user.task_set.filter(date=search_date)
        count = 0
        if k:
            for i in k:
                count+= int(i.hours)
                remainingHours = int(8-int(count))
        else:
            # For new user to reset 
            if user.task_set.count()== 0:
                remainingHour = -1

            remainingHours = 8
        
            messages.info(request,"No Records Found")
        l= Client.objects.filter(status='Active')
            
    else:
        msg = "Your section has expired"
        return render(request,"login.html",{"msg":msg})
    return render(request,'listtask.html',{'logos':logos,'k':k,'l':l,'d2':d2,'rd':rd,'w':w,'e':e,'u_d':u_d,'today':today,'rh':remainingHours,'re':remainingHours,'selectedDate':search_date,'kk':kk,'msg':message,'m':m,'d':d,'formatedSelectedDate': search_date_d,'messeges':messages,'pk':pk,'difference':difference})

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def employee_pending(request): 
    user= request.user
    kk=User.objects.filter(date=request.user.date) 
    
    for i in kk:
        dates=i.date.date()
       
    d2 = datetime.datetime.today().date()
    difference=(d2-dates).days
    
    pk=[]        
    today = datetime.datetime.today()
    
    numdays = difference
    for x in range (0, numdays):       
        date_ranges=dates + datetime.timedelta(days = x)        
        if date_ranges.weekday() < 6:
            pk.append(date_ranges)
    rm=[]
    for z in pk:
       
        k=user.task_set.filter(date=z)
        count = 0
        if k:
            for i in k:
                count+= int(i.hours)
                remainingHours = int(8-int(count))
        
                       
                # rm.append(z)
            rm.append(remainingHours)
             
        else:
            remainingHours = 8 
            # rm.append(z)
            rm.append(remainingHours)
         
    mylist=zip(pk,rm)
    my_lists=list(mylist)
    my_lists.reverse()
    # if not my_lists :
    #     messages.info(request,'No Records Found')
  
    
    my_list = [i for i in my_lists if i[1] > 0]
   
    page = request.GET.get('page', 1)
    paginator = Paginator(my_list, 20)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage:
        datas = paginator.page(paginator.num_pages)
    # html_content = render_to_string("pendingemail.html",{'first_name':user.first_name,"context":my_list})
    # text_content=strip_tags(html_content)
    # subject='Daily Timesheet Reminder'
    # # message='Hi  {0},your approved amount from {1} to {2} is Rs.{3} and billed amount as on {4} is Rs.{5}. \n '.format(user.first_name)
    # email_from=settings.EMAIL_HOST_USER
    # recipient_list=[user.email]
    # email = EmailMultiAlternatives(
    #         subject,
    #         text_content,
    #         email_from,
    #         recipient_list,
    #     )
    # email.attach_alternative(html_content,"text/html")
    # email.send()
    return render (request,'employeepending.html',{'logos':logos,'date':date,'pk':pk,'rm':rm,"context":my_list,'datas':datas})

 
@my_login_required
@allowed_users(allowed_roles=['Employee'])
def deletetask(request,id):
    u=Task.objects.get(id=id)
    u.delete()
    search_date = u.date.strftime("%Y-%m-%d")
    request.session['search_date'] = search_date
    return redirect('listtask')

@my_login_required
def log(request):
    #logout(request)
    #return redirect('loginf')
    return render(request,'login.html',{'logos':logos})

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def BillingPerfomance(request):
    user= request.user
    if user.is_authenticated:
        rank = user.rank
        des = user.desigenation
        mail =user.officemail
        userid=user.id
        h=0
        today = date.today().strftime("%Y-%m")
        r = date.today()
        start_date=date(int(r.year),int(r.month),1)
        k=User.objects.get(id=userid)
        print("NAME",k,rank)
        j=User.objects.filter(id=userid)
        for i in j:
            d=i.date_joined
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=start_date,date__lte=r)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                value=0
                Totalvalue=0
                messege="sorry you have no approved task in this month"
                return render(request,'justgageuser.html',{'des':des,'mail':mail,'messege':messege,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
        total_days_in_a_month=25
        total_working_hour_in_a_month=25*8
        numerator=per_hour*total_hour*100
        denominatior=per_hour* total_working_hour_in_a_month          
        value=numerator/denominatior
        if request.method=='POST':
            a=request.POST.get('date')
            print('date',a)
            y=a.split('-')[0]
            m=a.split('-')[1]
            year=int(y)
            month=int(m)
            num_days=(calendar.monthrange(year,month)[1]);
            end_date=date(int(y),int(m),num_days)
            start_date=date(int(y),int(m),1)
            for i in j:
                d=i.date_joined
                s=i.salary
                b=i.billing
                total=int(s)*int(b)
                per_day=total/25
                per_hour=per_day/8
                task=i.task_set.filter(status=True,date__gte=start_date,date__lte=end_date)
                if task:
                    for x in task:
                        h +=x.hours
                        total_hour=h
                    h=0
                else:
                    value=0
                    Totalvalue=0
                    messege="sorry you have no approved task in this month"
                    return render(request,'justgageuser.html',{'des':des,'mail':mail,'messege':messege,'rank':rank,'k':k,'value':value,'id':userid,'Totalvalue':Totalvalue,'searchDate':a})
            total_days_in_a_month=25
            total_working_hour_in_a_month=25*8
            numerator=per_hour*total_hour*100
            denominatior=per_hour* total_working_hour_in_a_month          
            value=numerator/denominatior
            print(" value",value)
            current_date=date.today()
            end_date=date(int(current_date.year),int(current_date.month),1)
            days_before=(end_date-timedelta(days=1))
            num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
            for i in j:
                s=i.salary
                b=i.billing
                total=int(s)*int(b)
                per_day=total/25
                per_hour=per_day/8
                task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
                if task:
                    for x in task:
                        h +=x.hours
                        total_hour=h
                    h=0
                else:
                    Totalvalue=0
                    return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':a})
            numerator=per_hour*total_hour*100
            denominatior=per_hour*num_months*200
            Totalvalue=numerator/denominatior
            print("Totalvalue",Totalvalue)
            return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'value':value,'id':userid,'searchDate':a,'Totalvalue':Totalvalue})
### for total performance ###
        current_date=date.today()
        end_date=date(int(current_date.year),int(current_date.month),1)
        days_before=(end_date-timedelta(days=1))
        num_months=(end_date.year-d.year)*12+(end_date.month-d.month)
        for i in j:
            s=i.salary
            b=i.billing
            total=int(s)*int(b)
            per_day=total/25
            per_hour=per_day/8
            task=i.task_set.filter(status=True,date__gte=d,date__lte=days_before)
            if task:
                for x in task:
                    h +=x.hours
                    total_hour=h
                h=0
            else:
                Totalvalue=0
                return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})
        numerator=per_hour*total_hour*100
        denominatior=per_hour*num_months*200
        Totalvalue=numerator/denominatior
        print("Totalvalue",Totalvalue)
    else:
        msg = "Your section has expired"
        return render(request,"login.html",{"msg":msg})
    return render(request,'justgageuser.html',{'des':des,'mail':mail,'rank':rank,'k':k,'id':userid,'value':value,'Totalvalue':Totalvalue,'searchDate':today})

@my_login_required
@allowed_users(allowed_roles=['Employee'])
def userincentive(request):
    user = request.user
    if user.is_authenticated:
        userid = user.id
        first_day_of_current_month = date.today().replace(day=1)
        last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
        b=last_day_of_previous_month.strftime('%Y-%m')
        mon=last_day_of_previous_month.strftime('%B')
        year=last_day_of_previous_month.year
        k= Incentives.objects.filter(employee=user,month=mon,year=year)
        if request.method =='POST':
            da = request.POST.get('searchdate')
            y = da.split('-')[0]
            m = da.split('-')[1]
            mo=date(int(y),int(m),1).strftime('%B')
            k=Incentives.objects.filter(employee=user,month=mo,year=y)
            return render(request,'userincentive.html',{'k':k,'selectedDate':da})
    else:
        msg = "Your section has expired"
        return render(request,'login.html',{'msg':msg})
    return render(request,'userincentive.html',{'k':k,'selectedDate':b})


@my_login_required
@allowed_users(allowed_roles=['Admin'])
def dashboard(request):
    z=User.objects.filter(status="Active",role="Admin")#active employee \
    x=User.objects.filter(status="Active",role="Employee")
    y=User.objects.filter(status="Active",role="Consultant")
    c=Client.objects.filter(status="Active")#active projects
    d=Estimate.objects.aggregate(sum_of_dueamount=Sum("dueamount"))#total of dueamount estimate
    q=Estimate.objects.aggregate(sum_of_paidamount=Sum("paidamount"))
    u=Estimate.objects.aggregate(sum_of_billedamount=Sum("billed_amount"))
    e=User.objects.filter(status="Active")#missing timesheet
    f=Engagement.objects.filter(status="Active")#active accounts
    g=Departments.objects.values_list('Dname')#no of departments
    k=My_task.objects.filter(assignee=request.user).exclude(status="Closed")
    o=My_task.objects.values_list('title').exclude(status="Closed")

   
    query=request.POST.get('search')
    users_data=getMissingUserDetails(query)

    if query:
        s=query
    else:
        s=''
    from collections import OrderedDict
    users_data = OrderedDict(sorted(users_data.items()))
    if not users_data:
        messages.info(request,'No Records Found')
    page = request.GET.get('page', 1)
    items = list(users_data.items())
    paginator = Paginator(items, 10)
    try:
        datas = paginator.page(page)
    except PageNotAnInteger:
        datas = paginator.page(1)
    except EmptyPage: 
        datas = paginator.page(paginator.num_pages)
    print("----------------",datas.paginator.count)
    return render(request,'dashboard.html',{'logos':logos,'z':z.count,'x':x.count,'y':y.count,'c':c.count,'d':d["sum_of_dueamount"],'q':q["sum_of_paidamount"],'u':u["sum_of_billedamount"],'datas':datas.paginator.count,'f':f.count,'g':g.count,'k':k.count,'o':o.count})

def error_404(request, exception):
    return render(request, '404.html')

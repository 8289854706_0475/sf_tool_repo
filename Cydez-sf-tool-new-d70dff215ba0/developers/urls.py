from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('create_task', views.create_task, name='create_task'),
    path('listtask', views.listtask, name='listtask'),
    path('employee_pending',views.employee_pending,name='employee_pending'),
    # path('listtask/<str:search_date>', views.listtask, name='listtask'),
    # path('searchtask', views.searchtask, name='searchtask'),
    path('deletetask/<int:id>', views.deletetask, name='deletetask'),
    path('log', views.log, name='log'),
    path('BillingPerfomance',views.BillingPerfomance,name="BillingPerfomance"),
    path('userincentive',views.userincentive,name="userincentive"),
    path('dashboard',views.dashboard,name="dashboard"),


]
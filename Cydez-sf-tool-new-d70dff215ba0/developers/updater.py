from django.shortcuts import render,redirect
from app1.models import Incentives
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from django.core.mail import EmailMessage,send_mail
from django.conf import settings
from app1.models import User
from datetime import datetime,timedelta,date
from apscheduler.schedulers.background import BackgroundScheduler
from django.core.mail import EmailMessage,send_mail
from django.conf import settings
from app1.models import User
from .models import Task
from io import BytesIO
from django.core.mail import send_mail
from django.core.mail  import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from time import time
import json
import random
import xlwt
from django.core.mail import EmailMessage
def weeklymail(request):
    from email.mime.text import MIMEText
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from django.core.mail import EmailMultiAlternatives
    from django.template.loader import render_to_string
    from email.mime.image import MIMEImage
    loop_count = 1
    import io 
    from io import BytesIO
    per_total = 0
    total_amount = 0
    employees={}
    l=list(Client.objects.filter(status="Active",weeklyMail=True).values('id'))
    l=Client.objects.filter(status="Active",weeklyMail=True)
    for x in l:
        for i in User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True):
            task = i.task_set.filter(project__id=x.id).values('employee__first_name','project__clientName','hours','amount')
            if task:
                for j in task:
                    per_total += j['amount']
                    o=per_total
                    money=o
                per_total=0
                df=pd.DataFrame(task)
                df2 = {'employee__first_name':'','project__clientName':'','hours':'Total','amount':o}
                df = df.append(df2, ignore_index = True)
                df.columns=['Developers','Project','Hours','Amount']
                employees[i.get_full_name()]=[o]
                total_amount += o
                loop_count += 1
                output = BytesIO()
                writer = pd.ExcelWriter(output, engine='xlsxwriter')
                df.to_excel(writer, sheet_name='Sheet' + str(loop_count),index=False)
                ### for adjust width ###
                worksheet=writer.sheets['Sheet' + str(loop_count)]
                for idx,col in enumerate(df):
                    series=df[col]
                    max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
                    worksheet.set_column(idx,idx,max_len)
                writer.save()
        subject = 'Weekly Report'
        html_body = render_to_string("email.html",{'total_amount':money})
        to=[x.email,"operations@cydeztechnologies.com"]
        msg = EmailMultiAlternatives(subject=subject, from_email=settings.EMAIL_HOST_USER,to=to)
        msg.attach_alternative(html_body, "text/html")
        msg.attach('file.xlsx', output.getvalue() , 'application/vnd.ms-excel')
        msg.send()
def testSchedule():
    
    
    pk=[]        
    today = datetime.datetime.today()
    print("p",today)
   
    for x in range (0, 5):       
        ranges=today - datetime.timedelta(days = x)
        date_ranges=ranges.date()
        if date_ranges.weekday() < 6:
            pk.append(date_ranges)
    rm=[]
    for i in User.objects.filter(status="Active",role="Employee"):
        k=i.task_set.filter(date=date_ranges)
        count = 0
        if k:
            for i in k:
                count+= int(i.hours)
                remainingHours = int(8-int(count))
        
                       
                # rm.append(z)
            rm.append(remainingHours)
             
        else:
            remainingHours = 8 
            # rm.append(z)
            rm.append(remainingHours)
         
    mylist=zip(pk,rm)
    my_lists=list(mylist)
    my_lists.reverse()
  
    
    my_list = [i for i in my_lists if i[1] > 0]
   
  
    html_content = render_to_string("pendingemail.html",{'first_name':i.first_name,"context":my_list})
    text_content=strip_tags(html_content)
    subject='Daily Timesheet Reminder'
    # message='Hi  {0},your approved amount from {1} to {2} is Rs.{3} and billed amount as on {4} is Rs.{5}. \n '.format(user.first_name)
    email_from=settings.EMAIL_HOST_USER
    recipient_list=[i.email]
    email = EmailMultiAlternatives(
            subject,
            text_content,
            email_from,
            recipient_list,
        )
    email.attach_alternative(html_content,"text/html")
	
    email.extra_headers = {'X-SMTPAPI': json.dumps({'send_at': time() + 120})}
    email.send()
# def testSchedule():
# 	import datetime
# 	date = datetime.date.today()
# 	day = date.strftime('%A')
# 	recipient_list = []
# 	if day != 'Sunday':
# 		# subject='Cydez Technologies'
# 		# message='Hi you have remaining task to add'
# 		# email_from=settings.EMAIL_HOST_USER
# 		for i in User.objects.filter(status="Active",role="Employee"):
# 			k=i.task_set.filter(date=date)
# 			count = 0
# 			remainingHours=8
# 			# if k:
# 			for j in k:
# 				count+= int(j.hours)
# 				remainingHours = int(8-int(count))
# 			if remainingHours > 0:
# 				if not i.email in recipient_list:
# 					recipient_list += [i.email]
# 		# send_mail(subject,message,email_from,recipient_list,fail_silently=True)
# 			subject, from_email, to = 'Weekly Timesheet Reminder',settings.EMAIL_HOST_USER,i.email

# 			html_content = render_to_string('pendingemail.html',{'user':subject} 
# 			) # render with dynamic value
# 			text_content = strip_tags(html_content) # Strip the html tag. So people can see the pure text at least.

# 			# create the email, and attach the HTML version as well.
# 			msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
# 			msg.attach_alternative(html_content, "text/html")
# 			from time import time
# 			import json
# 			msg.extra_headers = {'X-SMTPAPI': json.dumps({'send_at': time() + 60})}
# 			msg.send()
			
			
def incentiveSchedule():
	h=0
	subject='Cydez Technologies'
	email_from=settings.EMAIL_HOST_USER
	first_day_of_current_month = date.today().replace(day=1)
	last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
	first_day_of_previous_month = last_day_of_previous_month.replace(day=1)
	b=first_day_of_previous_month.strftime('%Y-%B')
	y=b.split('-')[0]
	m=b.split('-')[1]
	year=int(y)
	month=m
	j=User.objects.filter(status="Active",role="Employee")
	total_days_in_a_month=25
	for i in j:
		s=i.salary
		b=i.billing
		ran = i.rank
		inc = int(ran)*1000
		total=int(s)*int(b)
		per_day=total/25
		per_hour=per_day/8
		task=i.task_set.filter(status__contains="Approved",date__gte=first_day_of_previous_month,date__lte=last_day_of_previous_month)
		if task:
			for x in task:
				h +=x.hours
				total_hour=h
				numerator=per_hour*total_hour*100
				denominatior=per_day*total_days_in_a_month
				value=numerator/denominatior
			h=0
			if value >= 90:
				Incentives.objects.create(employee=i,incentiveAmount=inc,percentage=value,month=month,year=year)
				message='Hi you have {} incentive this month'.format(inc)
				send_mail(subject,message,email_from,[i.email],fail_silently=True)


def start():
	scheduler = BackgroundScheduler()
	scheduler.add_job(testSchedule, 'cron', hour='18', minute='00')
	scheduler.add_job(testSchedule, 'cron', hour='19', minute='00')
	scheduler.add_job(testSchedule, 'cron', hour='20', minute='00')
	# scheduler.add_job(weeklymail, 'cron',day_of_week='fri', hour='10', minute='00')
	if date.today().day == 1:
		scheduler.add_job(incentiveSchedule, 'cron', hour='06', minute='00')
	scheduler.start()

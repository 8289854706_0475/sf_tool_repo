from datetime import datetime
from pytz import utc

from django.shortcuts import render,redirect
from app1.models import Client, Incentives
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from django.core.mail import EmailMessage,send_mail
from django.conf import settings
from app1.models import User
from datetime import datetime,timedelta,date
from apscheduler.schedulers.background import BackgroundScheduler
from django.core.mail import EmailMessage,send_mail
from django.conf import settings
from app1.models import User
from io import BytesIO
from django.core.mail import send_mail
from django.core.mail  import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from time import time
import json
import random
import xlwt
from django.core.mail import EmailMessage
import pandas as pd
from app1.utils import getMissingUserDetails

from developers.models import Task
def weeklymail1():
    from email.mime.text import MIMEText
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from django.core.mail import EmailMultiAlternatives
    from django.template.loader import render_to_string
    from email.mime.image import MIMEImage
    loop_count = 1
    import io 
    from io import BytesIO
    per_total = 0
    total_amount = 0
    employees={}
    money=0
    output = None
    l=list(Client.objects.filter(status="Active",weeklyMail=True).values('id'))
    l=Client.objects.filter(status="Active",weeklyMail=True)
    for x in l:
        for i in User.objects.filter(status="Active",role="Employee").exclude(is_superuser=True):
            task = i.task_set.filter(project__id=x.id).values('employee__first_name','project__clientName','date','hours','amount')
            if task:
                for j in task:
                    per_total += j['amount']
                    o=per_total
                    money=o
                per_total=0
                df=pd.DataFrame(task)
                df2 = {'employee__first_name':'','project__clientName':'','date':'','hours':'Total','amount':o}
                df = df.append(df2, ignore_index = True)
                df.columns=['Employee','Project','Date','Hours','Amount']
                employees[i.get_full_name()]=[o]
                total_amount += o
                loop_count += 1
                output = BytesIO()
                writer = pd.ExcelWriter(output, engine='xlsxwriter')
                df.to_excel(writer, sheet_name='Sheet' + str(loop_count),index=False)
                ### for adjust width ###
                worksheet=writer.sheets['Sheet' + str(loop_count)]
                for idx,col in enumerate(df):
                    series=df[col]
                    max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
                    worksheet.set_column(idx,idx,max_len)
                writer.save()
        subject = 'Weekly Report'
        html_body = render_to_string("email.html",{'total_amount':money})
        # Need to remove abithadev2020@gmail.com this mail id after testing
        to=[x.email,"operations@cydeztechnologies.com","abithadev2020@gmail.com"]
        cc_project_manager=User.objects.filter(status="Active",role="Consultant",engagement__in= [x.Parent]).values_list('email',flat=True)
        msg = EmailMultiAlternatives(subject=subject, from_email=settings.EMAIL_HOST_USER,to=to,cc=cc_project_manager)
        msg.attach_alternative(html_body, "text/html")
        if output:
            msg.attach('file.xlsx', output.getvalue() , 'application/vnd.ms-excel')
        msg.send()
        
def weeklymail():
    from email.mime.text import MIMEText
    from email.mime.application import MIMEApplication
    from email.mime.multipart import MIMEMultipart
    from django.core.mail import EmailMultiAlternatives
    from django.template.loader import render_to_string
    from email.mime.image import MIMEImage
    
    print("I am executed..!")
    
    loop_count = 0
    import io 
    from io import BytesIO
    per_total = 0
    total_amount = 0
    employees={}
    money=0
    output = None
    today = date.today()
    # To filter task of previous week 
    check_date_start = today - timedelta(days = 12)
    check_date_end = today - timedelta(days = 6)
    l=list(Client.objects.filter(status="Active",weeklyMail=True).values('id'))
    l=Client.objects.filter(status="Active",weeklyMail=True)
    for x in l:

        total_amount = 0
        output = BytesIO()
        writer = pd.ExcelWriter(output, engine='xlsxwriter')
        loop_count += 1
        tasks =  Task.objects.filter(project__id=x.id,date__gte= check_date_start,date__lte= check_date_end,employee__status="Active",employee__role="Employee").exclude(employee__is_superuser=True).values('employee__first_name','project__clientName','date','taskdetails','hours','amount')
        df=pd.DataFrame(list(tasks),columns=['employee__first_name','project__clientName','date','taskdetails','hours','amount'])
        
        df.columns=['Employee','Project','Date','Task Details','Hours','Amount']
        
        for task in tasks:
            total_amount += task['amount']                   
   
        df2 = {'Employee':'','Project':'','Date':'','Task Details':'','Hours':'Total','Amount':total_amount}
        df = pd.concat([df, pd.DataFrame.from_records([df2])])
        df.to_excel(writer, sheet_name='Sheet' + str(loop_count),index=False)

        writer.save()
        
        subject = 'Weekly Report'
        html_body = render_to_string("email.html",{'total_amount':total_amount})
        # Need to remove abithadev2020@gmail.com this mail id after testing
        to=[x.email,"operations@cydeztechnologies.com","abithadev2020@gmail.com"]
        cc_project_manager=User.objects.filter(status="Active",role="Consultant",engagement__in= [x.Parent]).values_list('email',flat=True)
        msg = EmailMultiAlternatives(subject=subject, from_email=settings.EMAIL_HOST_USER,to=to,cc=cc_project_manager)
        msg.attach_alternative(html_body, "text/html")
        if total_amount!=0:
            msg.attach('file.xlsx', output.getvalue() , 'application/vnd.ms-excel')
        msg.send()
        
def timesheetRemindeSchedule():
    print("I am executed Timesheet Reminder..!")
    users_data=getMissingUserDetails(None)
    missing_details = list(users_data.items())
     
    for key, values in missing_details:

        my_list=[]
        missing_date=values['missing_date'].split(",")
        missing_hours=values['missing_hours'].split(",")
        absent_date=values['absent_date'].split(",")
        index = 0 
        for date in missing_date:
            if date.strip():
                my_list.append({'date':date,'hour':missing_hours[index]})
            index += 1
        index = 0 
        for date in absent_date:
            if date.strip():
                my_list.append({'date':date,'hour':'8'})
            index += 1

        html_content = render_to_string("pendingemail.html",{'first_name':values['first_name'],"context":my_list})
        text_content=strip_tags(html_content)
        subject='Daily Timesheet Reminder'
        # message='Hi  {0},your approved amount from {1} to {2} is Rs.{3} and billed amount as on {4} is Rs.{5}. \n '.format(user.first_name)
        email_from=settings.EMAIL_HOST_USER
        recipient_list=[values['email']]
        email = EmailMultiAlternatives(
                subject,
                text_content,
                email_from,
                recipient_list,
            )
        email.attach_alternative(html_content,"text/html")
        
        email.extra_headers = {'X-SMTPAPI': json.dumps({'send_at': time() + 120})}
        email.send()
# def testSchedule():
# 	import datetime
# 	date = datetime.date.today()
# 	day = date.strftime('%A')
# 	recipient_list = []
# 	if day != 'Sunday':
# 		# subject='Cydez Technologies'
# 		# message='Hi you have remaining task to add'
# 		# email_from=settings.EMAIL_HOST_USER
# 		for i in User.objects.filter(status="Active",role="Employee"):
# 			k=i.task_set.filter(date=date)
# 			count = 0
# 			remainingHours=8
# 			# if k:
# 			for j in k:
# 				count+= int(j.hours)
# 				remainingHours = int(8-int(count))
# 			if remainingHours > 0:
# 				if not i.email in recipient_list:
# 					recipient_list += [i.email]
# 		# send_mail(subject,message,email_from,recipient_list,fail_silently=True)
# 			subject, from_email, to = 'Weekly Timesheet Reminder',settings.EMAIL_HOST_USER,i.email

# 			html_content = render_to_string('pendingemail.html',{'user':subject} 
# 			) # render with dynamic value
# 			text_content = strip_tags(html_content) # Strip the html tag. So people can see the pure text at least.

# 			# create the email, and attach the HTML version as well.
# 			msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
# 			msg.attach_alternative(html_content, "text/html")
# 			from time import time
# 			import json
# 			msg.extra_headers = {'X-SMTPAPI': json.dumps({'send_at': time() + 60})}
# 			msg.send()
			
			
def incentiveSchedule():
	h=0
	subject='Cydez Technologies'
	email_from=settings.EMAIL_HOST_USER
	first_day_of_current_month = date.today().replace(day=1)
	last_day_of_previous_month = first_day_of_current_month - timedelta(days=1)
	first_day_of_previous_month = last_day_of_previous_month.replace(day=1)
	b=first_day_of_previous_month.strftime('%Y-%B')
	y=b.split('-')[0]
	m=b.split('-')[1]
	year=int(y)
	month=m
	j=User.objects.filter(status="Active",role="Employee")
	total_days_in_a_month=25
	for i in j:
		s=i.salary
		b=i.billing
		ran = i.rank
		inc = int(ran)*1000
		total=int(s)*int(b)
		per_day=total/25
		per_hour=per_day/8
		task=i.task_set.filter(status__contains="Approved",date__gte=first_day_of_previous_month,date__lte=last_day_of_previous_month)
		if task:
			for x in task:
				h +=x.hours
				total_hour=h
				numerator=per_hour*total_hour*100
				denominatior=per_day*total_days_in_a_month
				value=numerator/denominatior
			h=0
			if value >= 90:
				Incentives.objects.create(employee=i,incentiveAmount=inc,percentage=value,month=month,year=year)
				message='Hi you have {} incentive this month'.format(inc)
				send_mail(subject,message,email_from,[i.email],fail_silently=True)


def FirstCronTest():
	
	print("I am executed..!")